# NVIDIA BlueField-2 DOCA Sample Applications
![DOCA software Stack](doca-stack.jpg "DOCA Software Stack")
NVIDIA DOCA SDK provides a set of APIs and runtime components to enable networking, security and storage acceleration and isolation. DOCA is built on a number of industry-standard libraries like [DPDK](https://www.dpdk.org/) and [SPDK](https://spdk.io/) and provides a simplified abstraction layer for programming and offloading workloads to the BlueField-2 DPU.  

DOCA also provides APIs and runtime libraries to enable DPU specific hardware acceleration functions like encryption, hashing, or true random number generation. 

This repository is a collection of sample applications that leverage some of these DOCA libraries. 

For more information or to gain access to the DOCA SDK register for access on the [DOCA Developer Page](https://nvda.ws/3rMFfMM).

## Applications

Applications are located in the `applications` directory. Documentation can be found on the [DOCA SDK Applications Page](https://docs.nvidia.com/doca/sdk/index.html#applications)

* `/allreduce` - Allreduce is a collective operation which allows collecting data from different processing units to combine them into a global result by a chosen operator. In turn, the result is distributed back to all processing units.
* `/app_shield_agent` - App Shield Agent monitors a process in the host system using the DOCA App Shield library (doca-apsh).
* `/application_recognition` - Leverages the DPU hardware Regex engine to provide deep packet inspection and application recognition.
* `/dns_filter` - Demonstrates traffic steering capabilities to forward all DNS requests through the DPU's Arm CPU.
* `/east_west_overlay_encryption` - Script to deploy transparent IPsec functionality between DPUs. 
* `/file_scan` - Another use case for the Regex engine by scanning the contents of files for a matching regular expression.
* `/firewall` - A firewall application is a network security application that leverages the DPU's hardware capability to monitor incoming and outgoing network traffic and allow or block packets based on a set of preconfigured rules. The firewall application is based on DOCA Flow gRPC, used for remote programming of the DPU's hardware.
* `/ips` - Provides deep packet inspection that drops and logs packets matching specific regular expression patterns.
* `/l4_ovs_firewall` - L4 Open vSwitch (OVS) firewall is used to perform basic Access Control List (ACLs) operations. It allows to identify different flows based on L3/L4 headers and execute different actions.
* `/secure_channel` - DOCA Comm Channel is a secure, network independent communication channel between the host and the NVIDIA® BlueField® DPU.
* `/simple_fwd_vnf` - Basic connection tracking and forwarding acceleration. 
* `/url_filter` - Expands on the application recognition and DNS filtering capabilities to block traffic matching specific URLs.

## Samples

Commonly used code samples for DOCA libraries are located in the `samples` directory. Documentation can be found on the [DOCA SDK Samples Page](https://docs.nvidia.com/doca/sdk/index.html#samples)

* `/doca_apsh` - DOCA App Shield is a library for monitoring the host and authenticating the integrity of core processes.
* `/doca_comm_channel` - DOCA Comm Channel is a secure, network independent communication channel between the host and the BlueField-2 DPU.
* `/doca_dma` - DOCA direct memory access (DMA) library provides an API for copying data between buffers using hardware acceleration, supporting both local and remote memory regions.
* `/doca_dpi` - Deep packet inspection (DPI) is a method of examining the full content of data packets as they traverse a monitored network checkpoint.
* `/doca_flow` - DOCA Flow is the most fundamental API for building generic execution pipes in HW.
* `/doca_regex` - BlueField DPU supports high-speed hardware regular expression (RegEx) acceleration. This allows accelerating different security and networking applications which require RegEx processing.
* `/doca_telemetry` - DOCA Telemetry API offers a fast and convenient way to transfer user-defined data to DOCA Telemetry Service (DTS). In addition, the API provides several built-in outputs for user convenience, including saving data directly to storage, NetFlow, Fluent Bit forwarding, and Prometheus endpoint.


## Building and Deploying
To build and deploy the sample applications the DOCA SDK and an Arm build environment is required. By registering on the [DOCA Developer Page](https://nvda.ws/3rMFfMM) you can gain access to the SDK as well as a Docker container that provides Arm emulation, enabling software builds on x86 hardware.
