/*
 * Copyright (c) 2021-2022 NVIDIA CORPORATION & AFFILIATES, ALL RIGHTS RESERVED.
 *
 * This software product is a proprietary product of NVIDIA CORPORATION &
 * AFFILIATES (the "Company") and all right, title, and interest in and to the
 * software product, including all associated intellectual property rights, are
 * and shall remain exclusively with the Company.
 *
 * This software product is governed by the End User License Agreement
 * provided with the software product.
 *
 */

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>

#include <rte_sft.h>
#include <rte_malloc.h>
#include <rte_ring.h>

#include <doca_argp.h>
#include <doca_dpi.h>
#include <doca_log.h>

#include <dpdk_utils.h>
#include <utils.h>
#include <sig_db.h>

#include "ips_core.h"

DOCA_LOG_REGISTER(IPS);

/*
 * The main function, which does initialization and calls the per-lcore
 * functions.
 */

int
main(int argc, char *argv[])
{
	struct dpi_worker_attr dpi_worker_attr = {0};
	struct application_dpdk_config dpdk_config = {
		.port_config.nb_ports = 2,
		.port_config.nb_queues = 2,
		.port_config.nb_hairpin_q = 4,
		.sft_config = {
			.enable = true,
			.enable_ct = true,
			.enable_state_hairpin = false,
			.enable_state_drop = true
		},
		.reserve_main_thread = true,
	};
	struct ips_config ips_config = {.dpdk_config = &dpdk_config};

	/* init and start parsing */
	struct doca_argp_program_general_config *doca_argp_program_general_config;
	struct doca_argp_program_type_config type_config = {
		.is_dpdk = true,
		.is_grpc = false,
	};

	/* Parse cmdline/json arguments */
	doca_argp_init("ips", &type_config, &ips_config);
	register_ips_params();
	doca_argp_start(argc, argv, &doca_argp_program_general_config);

	/* init DPDK cores and sft */
	dpdk_init(&dpdk_config);

	ips_init(&dpdk_config, &ips_config, &dpi_worker_attr);

	/* Start the DPI processing */
	dpi_worker_lcores_run(dpdk_config.port_config.nb_queues, CLIENT_ID, dpi_worker_attr);

	force_quit = false;
	signal(SIGINT, signal_handler);
	signal(SIGTERM, signal_handler);

	/* The main thread */
	while (!force_quit) {
		sleep(1);
		if (ips_config.create_csv) {
			if (sig_database_write_to_csv(ips_config.csv_filename) != 0)
				APP_EXIT("CSV file access failed");
		}
		if (ips_config.netflow_source_id && send_netflow_record() != 0)
			APP_EXIT("Unexpected Netflow failure");
	}

	/* End of application flow */
	ips_destroy(&dpdk_config, &ips_config);

	/* DPDK cleanup */
	dpdk_fini(&dpdk_config);

	/* ARGP cleanup */
	doca_argp_destroy();

	return 0;
}
