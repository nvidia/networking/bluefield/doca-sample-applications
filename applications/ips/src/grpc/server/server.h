/*
 * Copyright (c) 2021 NVIDIA CORPORATION & AFFILIATES, ALL RIGHTS RESERVED.
 *
 * This software product is a proprietary product of NVIDIA CORPORATION &
 * AFFILIATES (the "Company") and all right, title, and interest in and to the
 * software product, including all associated intellectual property rights, are
 * and shall remain exclusively with the Company.
 *
 * This software product is governed by the End User License Agreement
 * provided with the software product.
 *
 */

#ifndef SERVER_H_
#define SERVER_H_

#include <grpcpp/grpcpp.h>

#include "ips.grpc.pb.h"

class IPSImpl : public IPS::Service {
	public:
		grpc::Status Subscribe(grpc::ServerContext *context, const SubscribeReq *request,
			grpc::ServerWriter<LogRecord> *writer) override;

		grpc::Status Quit(grpc::ServerContext *context, const QuitReq *request,
			QuitResp *QuitResponse) override;
};

#endif /* SERVER_H_ */
