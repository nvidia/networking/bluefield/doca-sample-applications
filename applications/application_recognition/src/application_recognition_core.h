/*
 * Copyright (c) 2021-2022 NVIDIA CORPORATION & AFFILIATES, ALL RIGHTS RESERVED.
 *
 * This software product is a proprietary product of NVIDIA CORPORATION &
 * AFFILIATES (the "Company") and all right, title, and interest in and to the
 * software product, including all associated intellectual property rights, are
 * and shall remain exclusively with the Company.
 *
 * This software product is governed by the End User License Agreement
 * provided with the software product.
 *
 */

#ifndef APPLICATION_RECOGNITION_CORE_H_
#define APPLICATION_RECOGNITION_CORE_H_

#include <doca_dpi.h>

#include <dpi_worker.h>
#include <offload_rules.h>

#ifdef __cplusplus
extern "C" {
#endif

#define CLIENT_ID 0x1A

#define MAX_FILE_NAME 255

struct ar_config {
	struct application_dpdk_config *dpdk_config;
	char cdo_filename[MAX_FILE_NAME];
	char csv_filename[MAX_FILE_NAME];
	bool print_on_match;
	bool create_csv;
	bool interactive_mode;
	bool enable_fragmentation;
	int netflow_source_id;

};

enum dpi_worker_action set_sig_db_on_match(int queue, const struct doca_dpi_result *result,
					uint32_t fid, void *user_data);

void ar_init(const struct application_dpdk_config *dpdk_config, struct ar_config *ar_config,
	     struct dpi_worker_attr *dpi_worker);

void ar_destroy(struct ar_config *ar);

void register_ar_params(void);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* APPLICATION_RECOGNITION_CORE_H_ */
