/*
 * Copyright (c) 2021-2022 NVIDIA CORPORATION & AFFILIATES, ALL RIGHTS RESERVED.
 *
 * This software product is a proprietary product of NVIDIA CORPORATION &
 * AFFILIATES (the "Company") and all right, title, and interest in and to the
 * software product, including all associated intellectual property rights, are
 * and shall remain exclusively with the Company.
 *
 * This software product is governed by the End User License Agreement
 * provided with the software product.
 *
 */

#include <string.h>

#include <doca_argp.h>

#include <utils.h>

#include "secure_channel_core.h"

#define MAX_MSG 10
#define SERVER_NAME "secure_channel_server"

DOCA_LOG_REGISTER(SECURE_CHANNEL::Core);

doca_error_t
secure_channel_client(struct doca_comm_channel_ep_t *ep, struct doca_comm_channel_addr_t **peer_addr,
		      struct secure_channel_config *app_cfg)
{
	char rcv_buf[MAX_MSG_SIZE];
	int client_msg_len = strlen(app_cfg->client_msg) + 1;
	int msg_count;
	size_t msg_len;
	doca_error_t ret;

	ret = doca_comm_channel_ep_connect(ep, SERVER_NAME, peer_addr);
	if (ret != DOCA_SUCCESS) {
		DOCA_LOG_ERR("Couldn't establish a connection with the server node: %s", doca_get_error_string(ret));
		return ret;
	}

	DOCA_LOG_INFO("Connection to server was established successfully");

	for (msg_count = 0; msg_count < app_cfg->nb_send_msg; msg_count++) {

		DOCA_LOG_DBG("Sending message to server: %s", app_cfg->client_msg);
		ret = doca_comm_channel_ep_sendto(ep, app_cfg->client_msg, client_msg_len, 0,
						  *peer_addr);
		if (ret != DOCA_SUCCESS) {
			DOCA_LOG_WARN("Message number %d was not sent: %s", msg_count,
				      doca_get_error_string(ret));
			continue;
		}
		msg_len = MAX_MSG_SIZE;
		ret = doca_comm_channel_ep_recvfrom(ep, (void *)rcv_buf, &msg_len, 0,
						    peer_addr);
		if (ret != DOCA_SUCCESS) {
			DOCA_LOG_WARN("Message was not received: %s", doca_get_error_string(ret));
			continue;
		}
		rcv_buf[MAX_MSG_SIZE - 1] = '\0';
		DOCA_LOG_INFO("Server response: %s", rcv_buf);
	}

	return DOCA_SUCCESS;
}

doca_error_t
secure_channel_server(struct doca_comm_channel_ep_t *ep, struct doca_comm_channel_addr_t **peer_addr)
{
	doca_error_t ret;
	size_t msg_len;
	char rcv_buf[MAX_MSG_SIZE];

	ret = doca_comm_channel_ep_listen(ep, SERVER_NAME);
	if (ret != DOCA_SUCCESS) {
		DOCA_LOG_ERR("Secure Channel server couldn't start listening: %s", doca_get_error_string(ret));
		return  ret;
	}

	DOCA_LOG_INFO("Server started Listening, waiting for new connections");

	while (1) {
		msg_len = MAX_MSG_SIZE;
		ret = doca_comm_channel_ep_recvfrom(ep, (void *)rcv_buf, &msg_len, 0,
						    peer_addr);
		if (ret != DOCA_SUCCESS) {
			DOCA_LOG_WARN("Message was not received: %s", doca_get_error_string(ret));
			continue;
		}

		rcv_buf[MAX_MSG_SIZE - 1] = '\0';
		DOCA_LOG_INFO("Received message: %s", rcv_buf);

		size_t nb_bytes = msg_len;

		ret = doca_comm_channel_ep_sendto(ep, rcv_buf, nb_bytes, 0, *peer_addr);
		if (ret != DOCA_SUCCESS)
			DOCA_LOG_WARN("Response was not sent successfully: %s", doca_get_error_string(ret));
	}
}

static void
client_mode_callback(void *config, void *param)
{
	struct secure_channel_config *app_cfg = (struct secure_channel_config *)config;

	if (app_cfg->mode != NO_VALID_INPUT) {
		DOCA_LOG_ERR("Secure Channel mode can be configured only once");
		doca_argp_usage();
	}

	app_cfg->mode = CLIENT;
}

static void
server_mode_callback(void *config, void *param)
{
	struct secure_channel_config *app_cfg = (struct secure_channel_config *)config;

	if (app_cfg->mode != NO_VALID_INPUT) {
		DOCA_LOG_ERR("Secure Channel mode can be configured only once");
		doca_argp_usage();
	}

	app_cfg->mode = SERVER;
}

static void
messages_amount_callback(void *config, void *param)
{
	struct secure_channel_config *app_cfg = (struct secure_channel_config *)config;

	int nb_send_msg = *(int *)param;

	if (nb_send_msg < 1) {
		DOCA_LOG_ERR("Amount of messages to be sent by the client is less than 1");
		doca_argp_usage();
	}

	app_cfg->nb_send_msg = nb_send_msg;
}

static void
message_callback(void *config, void *param)
{
	struct secure_channel_config *app_cfg = (struct secure_channel_config *)config;

	char *rcv_send_msg = (char *)param;

	int send_msg_len = strnlen(rcv_send_msg, MAX_MSG_SIZE);

	memcpy(app_cfg->client_msg, rcv_send_msg, send_msg_len);

	if (send_msg_len == MAX_MSG_SIZE) {
		DOCA_LOG_WARN("Message exceeded buffer size, trimming it to maximum size");
		app_cfg->client_msg[MAX_MSG_SIZE - 1] = '\0';
	}
}

static void
args_validation_callback(void *cfg, void *value)
{
	struct secure_channel_config *app_cfg = (struct secure_channel_config *)cfg;

	if (app_cfg->mode == NO_VALID_INPUT) {
		DOCA_LOG_ERR("Correct mode of operation is missing");
		doca_argp_usage();
	} else if (app_cfg->mode == SERVER && ((app_cfg->client_msg[0] != '\0') || (app_cfg->nb_send_msg != 0))) {
		DOCA_LOG_ERR(
			"Secure Channel server can not be configured with client message nor number of messages to be send");
		doca_argp_usage();
	} else if (app_cfg->mode == CLIENT && ((app_cfg->client_msg[0] == '\0') || (app_cfg->nb_send_msg == 0))) {
		DOCA_LOG_ERR(
			"Secure Channel client can not be configured without a message and the amount of messages to be send");
		doca_argp_usage();
	}
}

void
register_secure_channel_params()
{
	struct doca_argp_param client_mode_param = {
		.short_flag = "c",
		.long_flag = "client",
		.arguments = NULL,
		.description = "Runs secure channel end-point on client mode",
		.callback = client_mode_callback,
		.arg_type = DOCA_ARGP_TYPE_BOOLEAN,
		.is_mandatory = false,
		.is_cli_only = false
	};

	struct doca_argp_param server_mode_param = {
		.short_flag = "s",
		.long_flag = "server",
		.arguments = NULL,
		.description = "Runs secure channel end-point on server mode",
		.callback = server_mode_callback,
		.arg_type = DOCA_ARGP_TYPE_BOOLEAN,
		.is_mandatory = false,
		.is_cli_only = false
	};

	struct doca_argp_param message_param = {
		.short_flag = "m",
		.long_flag = "message",
		.arguments = NULL,
		.description = "Message to send by the client",
		.callback = message_callback,
		.arg_type = DOCA_ARGP_TYPE_STRING,
		.is_mandatory = false,
		.is_cli_only = false
	};

	struct doca_argp_param messages_amount_param = {
		.short_flag = "n",
		.long_flag = "num-msgs",
		.arguments = NULL,
		.description = "Number of messages to send by the client",
		.callback = messages_amount_callback,
		.arg_type = DOCA_ARGP_TYPE_INT,
		.is_mandatory = false,
		.is_cli_only = false
	};

	doca_argp_register_param(&client_mode_param);
	doca_argp_register_param(&server_mode_param);
	doca_argp_register_param(&message_param);
	doca_argp_register_param(&messages_amount_param);
	doca_argp_register_validation_callback(args_validation_callback);
	doca_argp_register_version_callback(sdk_version_callback);
}

void
configure_ep_qp_attributes(struct doca_comm_channel_init_attr *attr)
{
	attr->msgsize = MAX_MSG_SIZE;
	attr->maxmsgs = MAX_MSG;
	attr->cookie = 0;
	attr->flags = 0;
}

doca_error_t
create_secure_channel_ep(struct doca_comm_channel_ep_t **ep,
			 struct doca_comm_channel_init_attr *attr)
{
	doca_error_t ret;

	ret = doca_comm_channel_ep_create(attr, ep);

	if (ret != DOCA_SUCCESS)
		DOCA_LOG_ERR("Failed to create Secure Channel endpoint: %s", doca_get_error_string(ret));

	return ret;
}
