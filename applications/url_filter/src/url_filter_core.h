/*
 * Copyright (c) 2021-2022 NVIDIA CORPORATION & AFFILIATES, ALL RIGHTS RESERVED.
 *
 * This software product is a proprietary product of NVIDIA CORPORATION &
 * AFFILIATES (the "Company") and all right, title, and interest in and to the
 * software product, including all associated intellectual property rights, are
 * and shall remain exclusively with the Company.
 *
 * This software product is governed by the End User License Agreement
 * provided with the software product.
 *
 */

#ifndef URL_FILTER_CORE_H_
#define URL_FILTER_CORE_H_

#include <stdbool.h>
#include <stdint.h>
#include <unistd.h>

#include <dpi_worker.h>
#include <offload_rules.h>

#ifdef __cplusplus
extern "C" {
#endif

#define CLIENT_ID 0x1A

#define DEFAULT_TXT_INPUT "/tmp/signature.txt"
#define DEFAULT_CDO_OUTPUT "/tmp/signature.cdo"

typedef void (*print_callback)(uint32_t, const char *, uint32_t);

struct url_filter_config {
	struct application_dpdk_config *dpdk_config;
	bool print_on_match;
	bool enable_fragmentation;
};

void create_database(const char *signature_filename);

void compile_and_load_signatures(const char *signature_filename,
		const char *cdo_filename);

void create_url_signature(const char *signature_filename, const char *msg,
		const char *pcre);

void url_filter_init(const struct application_dpdk_config *dpdk_config,
		const struct url_filter_config *url_filter_config, struct dpi_worker_attr *dpi_worker);

void url_filter_destroy();

void register_url_params(void);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* URL_FILTER_CORE_H_ */
