/*
 * Copyright (c) 2021-2022 NVIDIA CORPORATION & AFFILIATES, ALL RIGHTS RESERVED.
 *
 * This software product is a proprietary product of NVIDIA CORPORATION &
 * AFFILIATES (the "Company") and all right, title, and interest in and to the
 * software product, including all associated intellectual property rights, are
 * and shall remain exclusively with the Company.
 *
 * This software product is governed by the End User License Agreement
 * provided with the software product.
 *
 */

#include <bsd/string.h>

#include <doca_argp.h>
#include <doca_log.h>

#include <utils.h>

#include "file_scan_core.h"

DOCA_LOG_REGISTER(FILE_SCAN::Core);

static void
init_regex_buffer(struct file_scan_config *app_cfg, struct doca_regex_device *regex_dev, struct doca_regex_buffer *buf)
{
	buf->address = app_cfg->data_buffer;
	buf->length = app_cfg->data_buffer_len;
	buf->has_mkey = 1; /* 1 means there is mkey */
	/* generate buf.mkey */
	if (doca_regex_buffer_generate_mkey(regex_dev, app_cfg->data_buffer, app_cfg->data_buffer_len, &buf->mkey) != 0)
		APP_EXIT("Unable to generate data mkey.");
}

static void
calculate_mempool_size(struct file_scan_config *app_cfg)
{
	long job_size = app_cfg->chunk_size > 0 ? app_cfg->chunk_size : app_cfg->data_buffer_len;

	app_cfg->mempool_size = MAX_MATCHES_PER_JOB * ((job_size / BF2_REGEX_JOB_LIMIT) + 1);
}

void
file_scan_init(struct file_scan_config *app_cfg)
{
	int ret;

	/* This apps uses 1 QP */
	app_cfg->nb_qps = 1;

	calculate_mempool_size(app_cfg);

	/* Create a doca reg instance. */
	app_cfg->doca_regex = doca_regex_create();
	if (app_cfg->doca_regex == NULL)
		APP_EXIT("Unable to create RegEx device.");

	app_cfg->regex_dev = doca_regex_create_pre_configured_regex_impl(RXP_DEVICE_TYPE);

	/* Init RegEx device. */
	ret = app_cfg->regex_dev->init_fn(app_cfg->regex_dev, app_cfg->pci_address);
	if (ret < 0)
		APP_EXIT("Unable to initialize RegEx device. [%s]", strerror(abs(ret)));

	/* Set the regex device as the main HW accelerator. */
	ret = doca_regex_hw_device_set(app_cfg->doca_regex, app_cfg->regex_dev);
	if (ret < 0)
		APP_EXIT("Unable to set RegEx device. [%s]", strerror(abs(ret)));

	/* Init matches memory pool. */
	app_cfg->matches_mp = doca_regex_mempool_create(sizeof(struct doca_regex_match), app_cfg->mempool_size);
	if (app_cfg->matches_mp == NULL)
		APP_EXIT("Unable to create matches mempool.");

	/* Configure QPs to memory pool. */
	ret = doca_regex_num_qps_set(app_cfg->doca_regex, app_cfg->nb_qps);
	if (ret < 0)
		APP_EXIT("Unable to configure %d QPs. [%s]", app_cfg->nb_qps, strerror(abs(ret)));

	/* Register QP-0 to the memory pool, this app uses only 1 QP. */
	ret = doca_regex_qp_mempool_set(app_cfg->doca_regex, app_cfg->matches_mp, REGEX_QP_INDEX);
	if (ret < 0)
		APP_EXIT("Unable to register pool with QP-%d. [%s]", REGEX_QP_INDEX, strerror(abs(ret)));

	/* Set Overlap in bytes for huge job(s). */
	ret = doca_regex_overlap_size_set(app_cfg->doca_regex, app_cfg->nb_overlap_bytes);
	if (ret < 0)
		APP_EXIT("Unable to set huge job emulation overlap size. [%s]", strerror(abs(ret)));

	/* Load compiled rules into the RegEx. */
	ret = doca_regex_program_compiled_rules_file(app_cfg->doca_regex, app_cfg->rules_file_path, NULL);
	if (ret < 0)
		APP_EXIT("Unable to program rules file. [%s]", strerror(abs(ret)));

	/* Start doca RegEx */
	ret = doca_regex_start(app_cfg->doca_regex);
	if (ret < 0)
		APP_EXIT("Unable to start doca RegEx. [%s]", strerror(abs(ret)));

	init_regex_buffer(app_cfg, app_cfg->regex_dev, &app_cfg->file_buffer);
}

void
file_scan_cleanup(struct file_scan_config *app_cfg)
{
	doca_regex_buffer_release_mkey(app_cfg->regex_dev);
	doca_regex_stop(app_cfg->doca_regex);
	doca_regex_destroy(app_cfg->doca_regex);
	doca_regex_mempool_destroy(app_cfg->matches_mp);
	app_cfg->doca_regex = NULL;
	if (app_cfg->regex_dev != NULL) {
		app_cfg->regex_dev->cleanup_fn(app_cfg->regex_dev);
		app_cfg->regex_dev->destroy_fn(app_cfg->regex_dev);
		app_cfg->regex_dev = NULL;
	}
	if (app_cfg->data_buffer)
		free(app_cfg->data_buffer);
	if (app_cfg->csv_fp)
		fclose(app_cfg->csv_fp);
}

/*
 * Returns file size in bytes
 * The function does not take ownership on the file (it does not close it!)
 * @param fp pointer to FILE
 * @return size of the given file in bytes.
 */
static long
file_get_size_in_bytes(FILE *fp)
{
	long result;

	if (fseek(fp, 0, SEEK_END) != 0)
		APP_EXIT("Could not determine file size.");

	result = ftell(fp);
	if (result < 0)
		APP_EXIT("Could not determine file size.");

	if (fseek(fp, 0, SEEK_SET) != 0)
		APP_EXIT("Could not restore file pointer after reading size.");

	return result;
}

static void
read_file(char *file_name, char **buffer_p, long *buffer_len)
{
	FILE *fp = fopen(file_name, "r");
	long bytes_read = 0;

	if (fp == NULL)
		APP_EXIT("Could not open file. [%s]", file_name);

	*buffer_len = file_get_size_in_bytes(fp);
	if (*buffer_len == 0) {
		fclose(fp);
		APP_EXIT("Specified file [%s] is empty!", file_name);
	}

	*buffer_p = malloc(sizeof(char) * (*buffer_len + 1));
	if (*buffer_p == NULL) {
		fclose(fp);
		APP_EXIT("Error reading file. [%s] !", file_name);
	}

	bytes_read = fread(*buffer_p, sizeof(char), *buffer_len, fp);
	if (bytes_read != *buffer_len) {
		free(*buffer_p);
		fclose(fp);
		APP_EXIT("Could not read the whole file. [%s] [%li bytes read]", file_name, bytes_read);
	}

	(*buffer_p)[bytes_read] = '\0';

	fclose(fp);
}

static void
rules_callback(void *config, void *param)
{
	struct file_scan_config *app_cfg = (struct file_scan_config *)config;
	char *rules_path = (char *)param;
	int len;

	len = strnlen(rules_path, MAX_FILE_NAME);
	if (len == MAX_FILE_NAME)
		APP_EXIT("Rules file name too long. Max %d", MAX_FILE_NAME - 1);
	strlcpy(app_cfg->rules_file_path, rules_path, MAX_FILE_NAME);
}

static void
data_callback(void *config, void *param)
{
	struct file_scan_config *app_cfg = (struct file_scan_config *)config;
	char *data_path = (char *)param;

	/* Read data file into the data buffer */
	read_file(data_path, &app_cfg->data_buffer, &app_cfg->data_buffer_len);
	if (app_cfg->data_buffer_len == 0)
		APP_EXIT("The provided data file is empty.");
}

static void
pci_address_callback(void *config, void *param)
{
	struct file_scan_config *app_cfg = (struct file_scan_config *)config;
	char *pci_address = (char *)param;
	int len;

	len = strnlen(pci_address, MAX_REGEX_PCI_ADDRESS_LEN);
	if (len == MAX_REGEX_PCI_ADDRESS_LEN)
		APP_EXIT("PCI file name too long. Max %d", MAX_REGEX_PCI_ADDRESS_LEN - 1);
	strlcpy(app_cfg->pci_address, pci_address, MAX_REGEX_PCI_ADDRESS_LEN);
}

static void
csv_callback(void *config, void *param)
{
	struct file_scan_config *app_cfg = (struct file_scan_config *)config;
	char *csv_path = (char *)param;

	if (csv_path == NULL)
		return;
	app_cfg->csv_fp = fopen(csv_path, "w");
	if (app_cfg->csv_fp == NULL) {
		DOCA_LOG_ERR("Failed to create CSV file. Skipping");
		return;
	}
	fprintf(app_cfg->csv_fp, "Line number,Match Index,Match Length,Rule Id\n");
}

static void
chunk_callback(void *config, void *param)
{
	struct file_scan_config *app_cfg = (struct file_scan_config *)config;
	int chunk = *(int *)param;

	if (chunk < 0)
		APP_EXIT("Chunk size must be > 0.");
	app_cfg->chunk_size = (uint32_t)chunk;
}

static void
overlap_callback(void *config, void *param)
{
	struct file_scan_config *app_cfg = (struct file_scan_config *)config;
	int overlap = *(int *)param;

	if (overlap < 0)
		APP_EXIT("Overlap size must be > 0.");
	app_cfg->nb_overlap_bytes = (uint32_t)overlap;
}

void
register_file_scan_params(void)
{
	struct doca_argp_param rules_param = {
		.short_flag = "r",
		.long_flag = "rules",
		.arguments = "<path>",
		.description = "Path to compiled rules file (rof2.binary)",
		.callback = rules_callback,
		.arg_type = DOCA_ARGP_TYPE_STRING,
		.is_mandatory = true,
		.is_cli_only = false,
	};
	struct doca_argp_param data_param = {
		.short_flag = "d",
		.long_flag = "data",
		.arguments = "<path>",
		.description = "Path to data file",
		.callback = data_callback,
		.arg_type = DOCA_ARGP_TYPE_STRING,
		.is_mandatory = true,
		.is_cli_only = false,
	};
	struct doca_argp_param pci_param = {
		.short_flag = "p",
		.long_flag = "pci-addr",
		.arguments = "<PCI-ADDRESS>",
		.description = "RegEx device PCI address",
		.callback = pci_address_callback,
		.arg_type = DOCA_ARGP_TYPE_STRING,
		.is_mandatory = true,
		.is_cli_only = false,
	};
	struct doca_argp_param csv_param = {
		.short_flag = "o",
		.long_flag = "output-csv",
		.arguments = "<path>",
		.description = "Path to the output of the CSV file",
		.callback = csv_callback,
		.arg_type = DOCA_ARGP_TYPE_STRING,
		.is_mandatory = false,
		.is_cli_only = false,
	};
	struct doca_argp_param chunk_param = {
		.short_flag = "c",
		.long_flag = "chunk-size",
		.arguments = "<Number of Bytes>",
		.description = "Chunk size of each job sent to the regex,  use 0 to send the file as 1 chunk.",
		.callback = chunk_callback,
		.arg_type = DOCA_ARGP_TYPE_INT,
		.is_mandatory = false,
		.is_cli_only = false,
	};
	struct doca_argp_param overlap_param = {
		.short_flag = "s",
		.long_flag = "overlap-size",
		.arguments = "<Number of Bytes>",
		.description = "Number of bytes of overlap to use for huge jobs",
		.callback = overlap_callback,
		.arg_type = DOCA_ARGP_TYPE_INT,
		.is_mandatory = false,
		.is_cli_only = false,
	};

	doca_argp_register_param(&rules_param);
	doca_argp_register_param(&data_param);
	doca_argp_register_param(&pci_param);
	doca_argp_register_param(&csv_param);
	doca_argp_register_param(&chunk_param);
	doca_argp_register_param(&overlap_param);
	doca_argp_register_version_callback(sdk_version_callback);
}
