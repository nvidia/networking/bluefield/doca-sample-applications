/*
 * Copyright (c) 2021-2022 NVIDIA CORPORATION & AFFILIATES, ALL RIGHTS RESERVED.
 *
 * This software product is a proprietary product of NVIDIA CORPORATION &
 * AFFILIATES (the "Company") and all right, title, and interest in and to the
 * software product, including all associated intellectual property rights, are
 * and shall remain exclusively with the Company.
 *
 * This software product is governed by the End User License Agreement
 * provided with the software product.
 *
 */

#ifndef DNS_FILTER_CORE_H_
#define DNS_FILTER_CORE_H_

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <unistd.h>

#ifdef GPU_SUPPORT
#include <cuda_runtime.h>
#endif

#include <doca_flow.h>
#include <doca_regex.h>

#include <offload_rules.h>

#ifdef __cplusplus
extern "C" {
#endif

#define MAX_FILE_NAME 255		/* Maximal length of file path */
#define MAX_REGEX_PCI_ADDRESS_LEN 32	/* Maximal length of regex pci address*/
#define MAX_REGEX_RESPONSE_SIZE 256	/* Maximal size of regex jobs response */
#define DNS_FILTER_MAX_FLOWS 1024	/* Maximal number of FLOWS in application pipes. */

enum dns_type_listing {
	DNS_NON_LISTING = 0,		/* Non listing type */
	DNS_ALLOW_LISTING,		/* Allowlist type */
	DNS_DENY_LISTING,		/* Denylist type */
};

struct dns_filter_config {
	struct doca_flow_pipe **drop_pipes;			/* Holds ports drop pipes */
	enum dns_type_listing listing_type;			/* Holds dns listing type */
	struct application_dpdk_config *dpdk_cfg;		/* App DPDK configuration struct */
	char pci_address[MAX_REGEX_PCI_ADDRESS_LEN];		/* Regex PCI address to use */
	char rules_file_path[MAX_FILE_NAME];			/* Path to Regex rules file */
	uint32_t regex_nb_qp;					/* Number of QP to use */
	struct doca_regex *doca_reg;				/* DOCA Regex interface */
	struct doca_regex_device *regex_dev;			/* DOCA Regex device interface */
	struct doca_regex_mempool **matches_mp;			/* DOCA Regex matches mempool */
};

struct dns_worker_ctx {
	int queue_id;				/* Queue ID */
	char **queries;				/* Holds DNS queries */
	struct dns_filter_config *app_cfg;	/* App config struct */
	struct doca_regex_job_response responses[MAX_REGEX_RESPONSE_SIZE];/* DOCA Regex jobs responses */

};

void dns_filter_init(struct dns_filter_config *app_cfg);
void dns_filter_destroy(struct dns_filter_config *app_cfg);
void dns_worker_lcores_run(struct dns_filter_config *app_cfg);
void register_dns_filter_params(void);
#ifdef GPU_SUPPORT
void workload_launch_gpu_processing(struct rte_gpu_comm_list *comm_list, cudaStream_t c_stream, char **queries);
#endif

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* DNS_FILTER_CORE_H_ */
