/*
 * Copyright (c) 2021-2022 NVIDIA CORPORATION & AFFILIATES, ALL RIGHTS RESERVED.
 *
 * This software product is a proprietary product of NVIDIA CORPORATION &
 * AFFILIATES (the "Company") and all right, title, and interest in and to the
 * software product, including all associated intellectual property rights, are
 * and shall remain exclusively with the Company.
 *
 * This software product is governed by the End User License Agreement
 * provided with the software product.
 *
 */
#include <arpa/nameser.h>
#include <bsd/string.h>
#include <getopt.h>
#include <netinet/in.h>
#include <resolv.h>
#include <signal.h>

#include <rte_errno.h>
#include <rte_ethdev.h>
#include <rte_malloc.h>
#include <rte_sft.h>

#include <doca_argp.h>
#include <doca_flow.h>
#include <doca_log.h>
#include <doca_regex_mempool.h>

#include <dpdk_utils.h>
#include <offload_rules.h>
#include <utils.h>

#include "dns_filter_core.h"

DOCA_LOG_REGISTER(DNS_FILTER::Core);

#define PACKET_BURST 128      /* The number of packets in the rx queue */
#define DNS_PORT 53	      /* DNS packet dst port */
#define UDP_HEADER_SIZE 8     /* UDP header size = 8 bytes (64 bits) */
#define MAX_PORT_STR_LEN 128  /* Maximal length of port name */
#define MAX_DNS_QUERY_LEN 512 /* Maximal length of DNS query */
#define REGEX_NB_QP 1	      /* number of regex QP's */
#define PACKET_MARKER 7	      /* Value for marking the matched packets */
#define DNS_PORTS_NUM 2

static bool force_quit;
#define DEFAULT_TIMEOUT_US (10000)
struct doca_flow_port *ports[DNS_PORTS_NUM];

static void
signal_handler(int signum)
{
	if (signum == SIGINT || signum == SIGTERM) {
		DOCA_LOG_INFO("Signal %d received, preparing to exit...", signum);
		force_quit = true;
	}
}

static void
regex_init(struct dns_filter_config *app_cfg)
{
	int ret, queue_id;

	/* Create a DOCA regex instance */
	app_cfg->doca_reg = doca_regex_create();
	if (app_cfg->doca_reg == NULL)
		APP_EXIT("DOCA regex creation Failed");

	/* Create a regex device instance */
	app_cfg->regex_dev = doca_regex_create_pre_configured_regex_impl("bf2");
	if (app_cfg->regex_dev == NULL)
		APP_EXIT("Unable to create RegEx device");

	/* Setup regex device instance */
	ret = app_cfg->regex_dev->init_fn(app_cfg->regex_dev, app_cfg->pci_address);
	if (ret < 0)
		APP_EXIT("Unable to init RegEx device");

	/* Set hw regex device to DOCA regex */
	ret = doca_regex_hw_device_set(app_cfg->doca_reg, app_cfg->regex_dev);
	if (ret < 0)
		APP_EXIT("Unable to install RegEx device");

	/* Set number of QP's to regex device */
	ret = doca_regex_num_qps_set(app_cfg->doca_reg, app_cfg->regex_nb_qp);
	if (ret < 0)
		APP_EXIT("Unable to configure %d QP", app_cfg->regex_nb_qp);

	/* Create regex match mempools, one mempool per QP */
	for (queue_id = 0; queue_id < app_cfg->regex_nb_qp; queue_id++) {
		app_cfg->matches_mp[queue_id] = doca_regex_mempool_create(sizeof(struct doca_regex_match), MAX_REGEX_RESPONSE_SIZE);
		if (app_cfg->matches_mp[queue_id] == NULL)
			APP_EXIT("Unable to create matches mempool");

		/* Attach matches mempool to regex device */
		ret = doca_regex_qp_mempool_set(app_cfg->doca_reg, app_cfg->matches_mp[queue_id], queue_id);
		if (ret < 0)
			APP_EXIT("Unable to register pool");
	}

	/* Attach rules file to DOCA regex */
	ret = doca_regex_program_compiled_rules_file(app_cfg->doca_reg, app_cfg->rules_file_path, NULL);
	if (ret < 0)
		APP_EXIT("Unable to program rules");

	/* Start doca regex */
	ret = doca_regex_start(app_cfg->doca_reg);
	if (ret < 0)
		APP_EXIT("Unable to start doca regex");
}

/*
 * Update DOCA flow entry with 5-tuple of current packet
 */
static void
update_packet_match(struct doca_flow_match *match, struct rte_mbuf *mbuf)
{
	struct rte_ipv4_hdr *ipv4_hdr_outer_l3 =
		rte_pktmbuf_mtod_offset(mbuf, struct rte_ipv4_hdr *, sizeof(struct rte_ether_hdr));
	uint8_t *ipv4_hdr = ((uint8_t *)ipv4_hdr_outer_l3 + rte_ipv4_hdr_len(ipv4_hdr_outer_l3));
	struct rte_udp_hdr *udp_hdr_outer_l4 = (typeof(udp_hdr_outer_l4))ipv4_hdr;

	/* Update the match field with packet 5-tuple */
	match->out_dst_ip.ipv4_addr = ipv4_hdr_outer_l3->dst_addr;
	match->out_src_ip.ipv4_addr = ipv4_hdr_outer_l3->src_addr;
	match->out_src_port = udp_hdr_outer_l4->src_port;
	print_header_info(mbuf, false, true, true);
}

static void
restrict_packet(struct dns_filter_config *app_cfg, int queue_id, struct rte_mbuf *mbuf)
{
	struct doca_flow_match match = {0};
	struct doca_flow_actions action = {0};
	struct doca_flow_error error = {0};
	struct doca_flow_pipe *pipe = app_cfg->drop_pipes[mbuf->port];
	struct doca_flow_pipe_entry *entry;

	/* Add hw entry to DNS drop pipe which presents current packet 5-tuple */
	DOCA_LOG_DBG("restricting packet:");
	update_packet_match(&match, mbuf);
	entry = doca_flow_pipe_add_entry(queue_id, pipe, &match, &action, NULL, NULL, 0, NULL, &error);
	if (entry == NULL)
		APP_EXIT("entry creation FAILED: %s", error.message);
}

#ifdef GPU_SUPPORT
/*
 * The main function for GPU workload(extracting DNS queries in parallel),
 * it prepares the relevant input to start a CUDA kernel
 * @pipe: a data structure holds GPU support details
 * @packets: array of mbufs, metadata for bursting packets
 * @nb_packets: packets array size
 * @queries: array of char*, holds the pointers to the DNS queies
 * NOTE: the array allocated in CPU memory and registered to GPU device to allow the access to it)
 */
static void
gpu_workload_run(struct gpu_pipeline *pipe, struct rte_mbuf **packets, uint16_t nb_packets, char **queries)
{
	int ret;
	enum rte_gpu_comm_list_status status;

	/* Update the communication list with informations from the list of mbufs (packet address, number of packets...) */
	ret = rte_gpu_comm_populate_list_pkts(pipe->comm_list, packets, nb_packets);
	if (ret != 0)
		APP_EXIT("rte_gpu_comm_populate_list_pkts returned error %d", ret);

	/* Wrapper function, create a CUDA kernal to start GPU workload */
	workload_launch_gpu_processing(pipe->comm_list, pipe->c_stream, queries);

	/* Waiting until GPU workload is done, comm_list.status will be updated once GPU done */
	do {
		/* Get the workload status using atomic function */
		ret = rte_gpu_comm_get_status(pipe->comm_list, &status);
		if (ret < 0)
			APP_EXIT("rte_gpu_comm_get_status error, killing the app.");
	} while (!force_quit && status != RTE_GPU_COMM_LIST_DONE);

	/* Check if happened any error in GPU side */
	CUDA_ERROR_CHECK(cudaGetLastError());
}
#else
/*
 * Helper function to exract DNS query per packet.
 * @pkt: packet to extract.
 * @query: a place where to store the pointer of DNS query.
 */
static void
extract_dns_query(struct rte_mbuf *pkt, char **query)
{
	int len, ret;
	ns_msg handle; /* nameserver struct for DNS packet */
	struct rte_mbuf mbuf = *pkt;
	struct rte_sft_error error;
	struct rte_sft_mbuf_info mbuf_info;
	uint32_t payload_offset = 0;
	const unsigned char *data;

	/* Parse mbuf, and extract the query */
	ret = rte_sft_parse_mbuf(&mbuf, &mbuf_info, NULL, &error);
	if (ret)
		APP_EXIT("rte_sft_parse_mbuf error: %s", error.message);

	/* Calculate the offset of UDP header start */
	payload_offset += ((mbuf_info.l4_hdr - (void *)mbuf_info.eth_hdr));

	/* Skip UDP header to get DNS (query) start */
	payload_offset += UDP_HEADER_SIZE;

	/* Get a pionter to start of packet payload */
	data = (const unsigned char *)rte_pktmbuf_adj(&mbuf, payload_offset);
	if (data == NULL)
		APP_EXIT("Error in pkt mbuf adj");
	len = rte_pktmbuf_data_len(&mbuf);

	/* Parse DNS packet information and fill them into handle fields */
	if (ns_initparse(data, len, &handle) < 0)
		APP_EXIT("Fail to parse domain DNS packet");

	/* Get DNS query start from handle field */
	*query = (char *)handle._sections[ns_s_qd];
}

/*
 * The main function for CPU workload, iterate on array of packets to extract the DNS queries.
 * @packets: array of mbufs, metadata for bursting packets.
 * @nb_packets: packets array size.
 * @queries: array of char*, holds the pointers to the DNS queies.
 */
static void
cpu_workload_run(struct rte_mbuf **packets, int nb_packets, char **queries)
{
	int i;

	for (i = 0; i < nb_packets; i++)
		extract_dns_query(packets[i], &queries[i]);
}
#endif

/*
 * In this function happened the inspection of DNS packets and classify if the query fit the listing type
 * The inspection includes extracting DNS query and set it to RegEx engine to check a match
 * @app_cfg: a pointer to app configuration struct
 * @packets_received: size of mbufs array
 * @packets: mbufs array
 */
void
regex_processing(struct dns_worker_ctx *worker_ctx, uint16_t packets_received, struct rte_mbuf **packets)
{
	int enq_job, deq_job, nb_jobs = 1;
	int ret, current_packet;
	struct doca_regex_buffer buf;
	struct dns_filter_config *app_cfg = worker_ctx->app_cfg;
	struct doca_regex_job_request job = {
		.rule_group_ids = {1, 0, 0, 0},
	};

	/* Start DNS workload */
#ifdef GPU_SUPPORT
	gpu_workload_run(&app_cfg->dpdk_cfg->pipe, packets, packets_received, worker_ctx->queries);
#else
	cpu_workload_run(packets, packets_received, worker_ctx->queries);
#endif

	/* Enqueue jobs to DOCA regex*/
	for (current_packet = 0; current_packet < packets_received; current_packet++) {

		/* Create regex job with data=dns-query */
		buf.address = (void *)worker_ctx->queries[current_packet];
		buf.length = strlen(worker_ctx->queries[current_packet]);

		/* Add to query mkey, permit regex memory access */
		buf.has_mkey = 1;
		ret = doca_regex_buffer_generate_mkey(app_cfg->regex_dev, worker_ctx->queries[current_packet], buf.length,
						      &buf.mkey);
		if (ret != 0)
			APP_EXIT("Failed to generate data mkey");

		/* Send a job for DNS query to DOCA regex */
		job.id = current_packet;
		job.buffer = &buf;
		enq_job = 0;
		do {
			enq_job += doca_regex_enqueue(app_cfg->doca_reg, worker_ctx->queue_id, &job, false);
			if (enq_job < 0)
				APP_EXIT("Failed to enqueue regex job");
		} while (enq_job < nb_jobs);
	}

	/* Dequeue jobs response from DOCA regex */
	deq_job = 0;
	do {
		ret = doca_regex_dequeue(app_cfg->doca_reg, worker_ctx->queue_id, worker_ctx->responses + deq_job,
					 packets_received - deq_job);
		if (ret < 0)
			APP_EXIT("Failed to dequeue regex job response");
		deq_job += ret;
	} while (deq_job < packets_received);
}

static void
free_regex_jobs(struct dns_worker_ctx *worker_ctx, uint16_t resp_received)
{
	int current_res, nb_matches;
	struct doca_regex_match *match, *next;
	struct doca_regex_mempool *mem_pool = worker_ctx->app_cfg->matches_mp[worker_ctx->queue_id];

	/* Iterate on RegEx respones and free all the matches */
	for (current_res = 0; current_res < resp_received; current_res++) {
		/* Free matches in mathces memory (DOCA regex) */
		nb_matches = worker_ctx->responses[current_res].num_matches;
		match = worker_ctx->responses[current_res].matches;
		while (nb_matches > 0) {
			next = match->next;
			doca_regex_mempool_obj_put(mem_pool, match);
			match = next;
			nb_matches--;
		}
	}
}

static int
filter_listing_packets(struct dns_worker_ctx *worker_ctx, uint16_t packets_received, char **valid_queries,
		       struct rte_mbuf **packets, struct rte_mbuf **packets_to_send)
{
	char *query;
	bool to_restrict;
	int packets_count = 0;
	uint32_t current_packet;
	struct rte_mbuf *packet;

	for (current_packet = 0; current_packet < packets_received; current_packet++) {
		packet = packets[worker_ctx->responses[current_packet].id];
		query = (char *)worker_ctx->queries[current_packet];
		switch (worker_ctx->app_cfg->listing_type) {
		case DNS_ALLOW_LISTING:
			/* Filtering as allowlist option */
			if (worker_ctx->responses[current_packet].num_matches != 0)
				to_restrict = false;
			else
				to_restrict = true;
			break;
		case DNS_DENY_LISTING:
			/* Filtering as denylist option */
			if (worker_ctx->responses[current_packet].num_matches == 0)
				to_restrict = false;
			else
				to_restrict = true;
			break;
		default:
			APP_EXIT("Invalid listing type");
			break;
		}
		if (to_restrict) {
			/* Blocking current packet */
			restrict_packet(worker_ctx->app_cfg, worker_ctx->queue_id, packet);
			DOCA_LOG_DBG("DNS query:  %s", query);
		} else {
			/* Hold packet and forward to the ingress port */
			packets_to_send[packets_count] = packet;
			valid_queries[packets_count++] = query;
		}
	}
	return packets_count;
}

/*
 * While application uploading and before adding offload rules for DNS packets,
 * non-DNS packets may be received to RX queues and the application will read them when calling (rte_eth_rx_burst ()).
 * Marking mode for matched packets is enabled in order to classify if the received packets were already
 * in the Rx queues before adding the rules.
 *
 * This function filters packets array to include only the marked packets (matched by one of the rules).
 */
static void
check_packets_marking(struct rte_mbuf **packets, uint16_t *packets_received)
{
	struct rte_mbuf *packet;
	uint32_t current_packet, index = 0;

	for (current_packet = 0; current_packet < *packets_received; current_packet++) {
		packet = packets[current_packet];
		if (packet->hash.fdir.hi == PACKET_MARKER) {
			/* Packet matched by one of pipe entries(rules) */
			packets[index] = packets[current_packet];
			index++;
			continue;
		}
		/* Packet didn't match by one of pipe entries(rules), packet received before rules offload */
		DOCA_LOG_WARN("Packet received before rules offload");
	}
	/* Packets array will contain marked packets in places < index */
	*packets_received = index;
}

static void
handle_packets_received(struct dns_worker_ctx *worker_ctx, uint16_t packets_received, struct rte_mbuf **packets)
{
	int packets_count;
	uint8_t ingress_port;
	uint32_t current_packet;
	struct rte_mbuf *packet = NULL;
	struct rte_mbuf *packets_to_send[PACKET_BURST] = {0};
	char *valid_queries[PACKET_BURST] = {0};

	/* Check packets marking */
	check_packets_marking(packets, &packets_received);
	if (packets_received == 0)
		return;

	/* Start regex jobs */
	regex_processing(worker_ctx, packets_received, packets);

	/* filter DNS packets depending to DOCA regex responses */
	packets_count = filter_listing_packets(worker_ctx, packets_received, valid_queries, packets, packets_to_send);

	for (current_packet = 0; current_packet < packets_count; current_packet++) {
		packet = packets[current_packet];
		/* Deciding the port to send the packet to */
		ingress_port = packet->port ^ 1;

		/* Print DNS packet info */
		print_header_info(packet, false, true, true);
		DOCA_LOG_DBG("DNS query:  %s", valid_queries[current_packet]);
	}
	if (packets_count > 0)
		/* Packet sent to port 0 or 1*/
		rte_eth_tx_burst(ingress_port, worker_ctx->queue_id, packets, packets_count);

	/* Clear matches mempool */
	free_regex_jobs(worker_ctx, packets_received);
}

static void
process_packets(struct dns_worker_ctx *worker_ctx, int ingress_port)
{
	struct rte_mbuf *packets[PACKET_BURST];
	int nb_packets = rte_eth_rx_burst(ingress_port, worker_ctx->queue_id, packets, PACKET_BURST);

	/* Handle the received packets from a queue with id = worker_ctx->queue_id */
	if (nb_packets) {
		DOCA_LOG_DBG("Received %d packets from port 0x%x using core %u", nb_packets, ingress_port, rte_lcore_id());
		handle_packets_received(worker_ctx, nb_packets, packets);
	}
}

static void
dns_filter_worker(void *args)
{
	struct dns_worker_ctx *worker_ctx = (struct dns_worker_ctx *) args;
	int ingress_port, nb_ports = worker_ctx->app_cfg->dpdk_cfg->port_config.nb_ports;

	DOCA_LOG_DBG("Core %u is receiving packets.", rte_lcore_id());
	while (!force_quit) {
		for (ingress_port = 0; ingress_port < nb_ports; ingress_port++)
			process_packets(worker_ctx, ingress_port);
	}

#ifdef GPU_SUPPORT
	/* Unregister the array of queries */
	int ret = rte_gpu_mem_unregister(worker_ctx->app_cfg->dpdk_cfg->pipe.gpu_id, worker_ctx->queries);

	if (ret < 0)
		APP_EXIT("GPU MEM unregistration failed with error [%d]", ret);
#endif
	rte_free(worker_ctx->queries);
	rte_free(worker_ctx);
}

void
dns_worker_lcores_run(struct dns_filter_config *app_cfg)
{

	uint16_t lcore_index = 0;
	int current_lcore = 0, nb_queues = app_cfg->dpdk_cfg->port_config.nb_queues;
	struct dns_worker_ctx *worker_ctx = NULL;

	DOCA_LOG_INFO("%d cores are used as workers", nb_queues);

	/* Init DNS workers to start processing packets */
	while ((current_lcore < RTE_MAX_LCORE) && (lcore_index < nb_queues)) {
		current_lcore = rte_get_next_lcore(current_lcore, true, false);

		/* Create worker context */
		worker_ctx = (struct dns_worker_ctx *)rte_zmalloc(NULL, sizeof(struct dns_worker_ctx), 0);
		if (worker_ctx == NULL)
			APP_EXIT("RTE malloc failed");
		worker_ctx->app_cfg = app_cfg;
		worker_ctx->queue_id = lcore_index;

		/* Create array of pointers (char*) to hold the queries */
		worker_ctx->queries = rte_zmalloc(NULL, PACKET_BURST * sizeof(char *), 0);
		if (worker_ctx->queries == NULL)
			APP_EXIT("Dynamic allocation failed");
	#ifdef GPU_SUPPORT
		/* Register external memory to GPU device, allow the access it */
		int ret = rte_gpu_mem_register(app_cfg->dpdk_cfg->pipe.gpu_id, PACKET_BURST, worker_ctx->queries);

		if (ret < 0)
			APP_EXIT("GPU MEM registration failed with error [%d]", ret);
	#endif
		/* Launch the worker to start process packets */
		if (rte_eal_remote_launch((void *)dns_filter_worker, (void *)worker_ctx, current_lcore) != 0)
			APP_EXIT("Remote launch failed");

		worker_ctx++;
		lcore_index++;
	}
}

static struct doca_flow_pipe *
build_drop_pipe(struct doca_flow_port *port, struct doca_flow_pipe *dns_pipe, uint16_t portid)
{
	struct doca_flow_pipe *drop_pipe;
	struct doca_flow_error err = {0};
	struct doca_flow_pipe_cfg drop_pipe_cfg = {0};
	struct doca_flow_match drop_match;
	struct doca_flow_actions actions;
	struct doca_flow_fwd drop_fw;
	struct doca_flow_fwd drop_miss_fw;

	/* Allocate DNS drop pipe fields */
	memset(&actions, 0, sizeof(actions));
	memset(&drop_fw, 0, sizeof(drop_fw));
	memset(&drop_miss_fw, 0, sizeof(drop_miss_fw));
	memset(&drop_match, 0, sizeof(drop_match));
	memset(&drop_pipe_cfg, 0, sizeof(drop_pipe_cfg));

	drop_pipe_cfg.name = "DNS_DROP_PIPE";
	drop_pipe_cfg.type = DOCA_FLOW_PIPE_BASIC;
	drop_pipe_cfg.match = &drop_match;
	drop_pipe_cfg.actions = &actions;
	drop_pipe_cfg.port = port;
	drop_pipe_cfg.is_root = true;

	drop_match.out_dst_ip.ipv4_addr = 0xffffffff;
	drop_match.out_src_ip.ipv4_addr = 0xffffffff;
	drop_match.out_dst_ip.type = DOCA_FLOW_IP4_ADDR;
	drop_match.out_src_ip.type = DOCA_FLOW_IP4_ADDR;
	drop_match.out_l4_type = DOCA_PROTO_UDP;
	drop_match.out_dst_port = rte_cpu_to_be_16(DNS_PORT);
	drop_match.out_src_port = 0xffff;

	drop_fw.type = DOCA_FLOW_FWD_DROP;

	drop_miss_fw.type = DOCA_FLOW_FWD_PIPE;
	drop_miss_fw.next_pipe = dns_pipe;

	/* Create DNS drop pipe */
	drop_pipe = doca_flow_create_pipe(&drop_pipe_cfg, &drop_fw, &drop_miss_fw, &err);
	if (drop_pipe == NULL)
		APP_EXIT("DNS pipe creation FAILED: %s", err.message);
	return drop_pipe;
}

/* Builds DNS flow pipe for every port */
static struct doca_flow_pipe *
build_dns_pipe(struct dns_filter_config *app_cfg, struct doca_flow_port *port, struct doca_flow_pipe *hairpin_pipe)
{
	int queue_index, nb_queues = app_cfg->dpdk_cfg->port_config.nb_queues;
	struct doca_flow_match dns_match;
	struct doca_flow_fwd dns_fw, dns_miss_fw;
	struct doca_flow_actions actions;
	struct doca_flow_pipe_cfg dns_pipe_cfg = {0};
	struct doca_flow_pipe *dns_pipe;
	struct doca_flow_error err = {0};
	uint16_t rss_queues[nb_queues];
	struct doca_flow_pipe_entry *entry;

	/* Allocate DNS pipe fields */
	memset(&actions, 0, sizeof(actions));
	memset(&dns_fw, 0, sizeof(dns_fw));
	memset(&dns_miss_fw, 0, sizeof(dns_miss_fw));
	memset(&dns_match, 0, sizeof(dns_match));
	memset(&dns_pipe_cfg, 0, sizeof(dns_pipe_cfg));

	dns_pipe_cfg.name = "DNS_FW_PIPE";
	dns_pipe_cfg.type = DOCA_FLOW_PIPE_BASIC;
	dns_pipe_cfg.match = &dns_match;
	dns_pipe_cfg.port = port;
	dns_pipe_cfg.actions = &actions;

	dns_match.out_dst_ip.type = DOCA_FLOW_IP4_ADDR;
	dns_match.out_l4_type = IPPROTO_UDP;
	dns_match.out_dst_port = rte_cpu_to_be_16(DNS_PORT);

	/* Configure queues for rss fw */
	for (queue_index = 0; queue_index < nb_queues; queue_index++)
		rss_queues[queue_index] = queue_index;

	dns_fw.type = DOCA_FLOW_FWD_RSS;
	dns_fw.rss_queues = rss_queues;
	dns_fw.rss_flags = DOCA_FLOW_RSS_UDP;
	dns_fw.num_of_queues = nb_queues;
	dns_fw.rss_mark = PACKET_MARKER;

	/* Configure miss fwd for non DNS packets */
	dns_miss_fw.type = DOCA_FLOW_FWD_PIPE;
	dns_miss_fw.next_pipe = hairpin_pipe;
	dns_pipe = doca_flow_create_pipe(&dns_pipe_cfg, &dns_fw, &dns_miss_fw, &err);
	if (dns_pipe == NULL)
		APP_EXIT("DNS pipe creation FAILED: %s", err.message);

	/* Add HW offload DNS rule */
	entry = doca_flow_pipe_add_entry(0, dns_pipe, &dns_match, &actions, NULL, NULL, 0, NULL, &err);
	if (entry == NULL)
		APP_EXIT("entry creation FAILED: %s", err.message);

	return dns_pipe;
}

static struct doca_flow_pipe *
hairpin_non_dns_packets(struct doca_flow_port *port, uint16_t port_id)
{
	struct doca_flow_match non_dns_match;
	struct doca_flow_fwd non_dns_fw;
	struct doca_flow_actions actions;
	struct doca_flow_pipe_cfg non_dns_pipe_cfg = {0};
	struct doca_flow_pipe *non_dns_pipe;
	struct doca_flow_error err = {0};
	struct doca_flow_pipe_entry *entry;

	/* Zeroed fields are ignored , no changeable fields */
	memset(&non_dns_match, 0, sizeof(non_dns_match));
	memset(&actions, 0, sizeof(actions));
	memset(&non_dns_fw, 0, sizeof(non_dns_fw));
	memset(&non_dns_pipe_cfg, 0, sizeof(non_dns_pipe_cfg));
	non_dns_pipe_cfg.name = "HAIRPIN_NON_DNS_PIPE";
	non_dns_pipe_cfg.type = DOCA_FLOW_PIPE_BASIC;
	non_dns_pipe_cfg.match = &non_dns_match;

	/* Configure the port id "owner" of pipe */
	non_dns_pipe_cfg.port = port;
	non_dns_pipe_cfg.actions = &actions;
	non_dns_fw.type = DOCA_FLOW_FWD_PORT;
	non_dns_fw.port_id = port_id ^ 1;

	non_dns_pipe = doca_flow_create_pipe(&non_dns_pipe_cfg, &non_dns_fw, NULL, &err);
	if (non_dns_pipe == NULL)
		APP_EXIT("failed to create non-DNS pipe: %s", err.message);
	entry = doca_flow_pipe_add_entry(0, non_dns_pipe, &non_dns_match, &actions, NULL, NULL, 0, NULL, &err);
	if (entry == NULL)
		APP_EXIT("entry creation FAILED: %s", err.message);

	return non_dns_pipe;
}

/* Create doca flow ports */
struct doca_flow_port *
dns_filter_port_create(uint8_t portid)
{
	char port_id_str[MAX_PORT_STR_LEN];
	struct doca_flow_error err = {0};
	struct doca_flow_port *port;
	struct doca_flow_port_cfg port_cfg = {0};

	port_cfg.port_id = portid;
	port_cfg.type = DOCA_FLOW_PORT_DPDK_BY_ID;
	snprintf(port_id_str, MAX_PORT_STR_LEN, "%d", port_cfg.port_id);
	port_cfg.devargs = port_id_str;
	port = doca_flow_port_start(&port_cfg, &err);
	if (port == NULL)
		APP_EXIT("failed to initialize doca flow port: %s", err.message);
	return port;
}

/* Initialize doca flow ports */
void
dns_filter_destroy_ports(int nb_ports)
{
	int portid;

	for (portid = 0; portid < nb_ports; portid++) {
		if (ports[portid])
			doca_flow_destroy_port(portid);
	}
}

/* Initialize doca flow ports */
int
dns_filter_init_ports(int nb_ports)
{
	int portid, ret;

	for (portid = 0; portid < nb_ports; portid++) {
		/* Create doca flow port */
		ports[portid] = dns_filter_port_create(portid);
		/* Pair ports should same as dpdk hairpin binding order */
		if (!portid || !(portid % 2))
			continue;
		ret = doca_flow_port_pair(ports[portid], ports[portid ^ 1]);
		if (ret < 0) {
			DOCA_LOG_ERR("pair port %u %u fail", portid, portid ^ 1);
			dns_filter_destroy_ports(portid + 1);
			return ret;
		}
	}
	return 0;
}

void
dns_filter_init(struct dns_filter_config *app_cfg)
{
	uint16_t portid, nb_ports;
	struct doca_flow_error err = {0};
	struct doca_flow_cfg dns_flow_cfg = {0};
	struct doca_flow_pipe *hairpin_pipe;
	struct doca_flow_pipe *dns_pipe;
	struct application_dpdk_config *dpdk_config = app_cfg->dpdk_cfg;

	signal(SIGINT, signal_handler);
	signal(SIGTERM, signal_handler);

	/* Initialize doca framework */
	dns_flow_cfg.queues = dpdk_config->port_config.nb_queues;
	dns_flow_cfg.mode_args = "vnf";
	app_cfg->regex_nb_qp = dpdk_config->port_config.nb_queues;
	nb_ports = dpdk_config->port_config.nb_ports;

	/* Create mempools as number of queues */
	app_cfg->matches_mp = malloc(sizeof(*app_cfg->matches_mp) * dpdk_config->port_config.nb_queues);
	if (app_cfg->matches_mp == NULL)
		APP_EXIT("Failed to allocate mempools array for DNS RegEx QPs");

	/* Create array to hold drop pipe per port */
	app_cfg->drop_pipes = malloc(sizeof(struct doca_flow_pipe *) * nb_ports);
	if (app_cfg->drop_pipes == NULL)
		APP_EXIT("Failed to allocate memory for DNS drop pipes");

	if (doca_flow_init(&dns_flow_cfg, &err) < 0) {
		free(app_cfg->drop_pipes);
		APP_EXIT("failed to init doca: %s", err.message);
	}

	if (dns_filter_init_ports(nb_ports) < 0) {
		free(app_cfg->drop_pipes);
		doca_flow_destroy();
		APP_EXIT("failed to init ports");
	}

	for (portid = 0; portid < nb_ports; portid++) {
		/* Hairpin pipes for non-DNS packets */
		hairpin_pipe = hairpin_non_dns_packets(ports[portid], portid);

		/* DNS flow pipe */
		dns_pipe = build_dns_pipe(app_cfg, ports[portid], hairpin_pipe);

		/* DNS drop pipe */
		app_cfg->drop_pipes[portid] = build_drop_pipe(ports[portid], dns_pipe, portid);
	}
	/* DOCA regex initialization */
	regex_init(app_cfg);
	DOCA_LOG_DBG("Application configuration and rules offload done");
}

void
dns_filter_destroy(struct dns_filter_config *app_cfg)
{
	int i, nb_queues = app_cfg->dpdk_cfg->port_config.nb_queues;

	/* Cleanup DOCA regex resources */
	doca_regex_buffer_release_mkey(app_cfg->regex_dev);
	doca_regex_stop(app_cfg->doca_reg);
	for (i = 0; i < nb_queues; i++)
		doca_regex_mempool_destroy(app_cfg->matches_mp[i]);
	free(app_cfg->matches_mp);
	doca_regex_destroy(app_cfg->doca_reg);

	if (app_cfg->regex_dev != NULL) {
		app_cfg->regex_dev->cleanup_fn(app_cfg->regex_dev);
		app_cfg->regex_dev->destroy_fn(app_cfg->regex_dev);
		app_cfg->regex_dev = NULL;
	}
	app_cfg->doca_reg = NULL;
	doca_flow_destroy();
	if (app_cfg->drop_pipes != NULL)
		free(app_cfg->drop_pipes);
}

static void
dns_filter_args_validation_callback(void *config, void *param)
{
	struct dns_filter_config *dns_cfg = (struct dns_filter_config *)config;

	if (dns_cfg->listing_type == DNS_NON_LISTING) {
		DOCA_LOG_ERR("Missing dns listing type");
		doca_argp_usage();
	}
}

static void
allowlist_callback(void *config, void *param)
{
	struct dns_filter_config *dns_cfg = (struct dns_filter_config *)config;
	char *rules_path = (char *)param;

	if (dns_cfg->listing_type != DNS_NON_LISTING) {
		DOCA_LOG_ERR("Already inserted denylisting type");
		doca_argp_usage();
	}
	dns_cfg->listing_type = DNS_ALLOW_LISTING;
	if (strnlen(rules_path, MAX_FILE_NAME) == MAX_FILE_NAME)
		APP_EXIT("Allowlist rules file name too long max %d\n", MAX_FILE_NAME - 1);
	strlcpy(dns_cfg->rules_file_path, rules_path, MAX_FILE_NAME);
}

static void
denylist_callback(void *config, void *param)
{
	struct dns_filter_config *dns_cfg = (struct dns_filter_config *)config;
	char *rules_path = (char *)param;

	if (dns_cfg->listing_type != DNS_NON_LISTING) {
		DOCA_LOG_ERR("Already inserted allowlisting type");
		doca_argp_usage();
	}
	dns_cfg->listing_type = DNS_DENY_LISTING;
	if (strnlen(rules_path, MAX_FILE_NAME) == MAX_FILE_NAME)
		APP_EXIT("Denylist rules file name too long max %d\n", MAX_FILE_NAME - 1);
	strlcpy(dns_cfg->rules_file_path, rules_path, MAX_FILE_NAME);
}

static void
pci_address_callback(void *config, void *param)
{
	struct dns_filter_config *dns_cfg = (struct dns_filter_config *)config;
	char *pci_address = (char *)param;

	if (strnlen(pci_address, MAX_REGEX_PCI_ADDRESS_LEN) == MAX_REGEX_PCI_ADDRESS_LEN)
		APP_EXIT("Pci address too long max %d\n", MAX_REGEX_PCI_ADDRESS_LEN - 1);
	strlcpy(dns_cfg->pci_address, pci_address, MAX_REGEX_PCI_ADDRESS_LEN);
}

void
register_dns_filter_params(void)
{
	struct doca_argp_param allowlist_param = {
		.short_flag = "a",
		.long_flag = "allowlist",
		.arguments = "<path>",
		.description = "Path to allowlist rules file (rof2.binary)",
		.callback = allowlist_callback,
		.arg_type = DOCA_ARGP_TYPE_STRING,
		.is_mandatory = false,
		.is_cli_only = false
	};
	struct doca_argp_param denylist_param = {
		.short_flag = "d",
		.long_flag = "denylist",
		.arguments = "<path>",
		.description = "Path to denylist rules file (rof2.binary)",
		.callback = denylist_callback,
		.arg_type = DOCA_ARGP_TYPE_STRING,
		.is_mandatory = false,
		.is_cli_only = false
	};
	struct doca_argp_param pci_address_param = {
		.short_flag = "p",
		.long_flag = "pci-addr",
		.arguments = "<address>",
		.description = "Set PCI address of the RXP engine to use",
		.callback = pci_address_callback,
		.arg_type = DOCA_ARGP_TYPE_STRING,
		.is_mandatory = true,
		.is_cli_only = false
	};

	doca_argp_register_param(&allowlist_param);
	doca_argp_register_param(&denylist_param);
	doca_argp_register_param(&pci_address_param);
	doca_argp_register_version_callback(sdk_version_callback);
	doca_argp_register_validation_callback(dns_filter_args_validation_callback);
}
