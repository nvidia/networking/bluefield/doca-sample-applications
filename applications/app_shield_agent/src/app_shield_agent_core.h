/*
 * Copyright (c) 2021-2022 NVIDIA CORPORATION & AFFILIATES, ALL RIGHTS RESERVED.
 *
 * This software product is a proprietary product of NVIDIA CORPORATION &
 * AFFILIATES (the "Company") and all right, title, and interest in and to the
 * software product, including all associated intellectual property rights, are
 * and shall remain exclusively with the Company.
 *
 * This software product is governed by the End User License Agreement
 * provided with the software product.
 *
 */

#ifndef APP_SHIELD_AGENT_CORE_H_
#define APP_SHIELD_AGENT_CORE_H_

#include <doca_apsh.h>
#include <doca_dev.h>
#include <doca_telemetry.h>


#ifdef __cplusplus
extern "C" {
#endif

#define DOCA_DEVINFO_VUID_LEN 128  /* See DOCA_DEVINFO_REMOTE_PROPERTY_VUID in doca_dev.h */
#define MAX_DMA_IBDEV_LEN 64	   /* See DOCA_DEVINFO_PROPERTY_IBDEV_NAME in doca_dev.h */
/*
 * The path is read from the host memory, from the OS process structs.
 * In the linux case the path is actually the process "comm" which max len is 16.
 * In Windows the path is actually the process "image_file_name" which unofficial sources are saying is 0x20 bytes long,
 * the official doc refer only to a full path to file and is saying the default MAX_PATH value is 260 (can be changed).
 */
#define MAX_PATH 260

struct apsh_config {
	unsigned int pid;
	char exec_hash_map_path[MAX_PATH];
	char system_mem_region_path[MAX_PATH];
	char system_vuid[DOCA_DEVINFO_VUID_LEN + 1];
	char dma_dev_name[MAX_DMA_IBDEV_LEN + 1];
	char system_os_symbol_map_path[MAX_PATH];
	enum doca_apsh_system_os os_type;
	int time_interval;
};

struct apsh_resources {
	struct doca_apsh_ctx *ctx;
	struct doca_apsh_system *sys;
	struct doca_dev_remote *system_device;
	struct doca_dev *dma_device;
};

/* Event struct from which report will be serialized */
struct attestation_event {
	doca_telemetry_timestamp_t  timestamp;  /*< Timestamp of when the scan and the validation were completed */
	int32_t                     pid;  /*< Process id number that have been scanned */
	int32_t                     result;  /*< The end result of the scan, 0 on uncompromising, error otherwise */
	uint64_t                    scan_count;  /*< This scan number, begining with 0 */
	char                        path[MAX_PATH + 1];  /*< The path of that process  */
} __attribute__((packed));

struct event_indexes {
	doca_telemetry_type_index_t attest_index;
};

void register_apsh_params(void);
void app_shield_agent_init(struct apsh_config *conf, struct apsh_resources *resrc);
void app_shield_agent_cleanup(struct apsh_resources *resrc);
struct doca_apsh_process *get_process_by_pid(struct doca_apsh_process ***pslist, struct apsh_resources *resrc,
					     struct apsh_config *apsh_conf);
int telemetry_start(void **telemetry_schema, void **telemetry_source, struct event_indexes *indices);
void telemetry_destroy(void *telemetry_scheme, void *telemetry_source);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* APP_SHIELD_AGENT_CORE_H_ */
