/*
 * Copyright (c) 2021-2022 NVIDIA CORPORATION & AFFILIATES, ALL RIGHTS RESERVED.
 *
 * This software product is a proprietary product of NVIDIA CORPORATION &
 * AFFILIATES (the "Company") and all right, title, and interest in and to the
 * software product, including all associated intellectual property rights, are
 * and shall remain exclusively with the Company.
 *
 * This software product is governed by the End User License Agreement
 * provided with the software product.
 *
 */
#include <stdlib.h>
#include <bsd/string.h>
#include <unistd.h>

#include <doca_argp.h>
#include <doca_log.h>

#include <utils.h>

#include "app_shield_agent_core.h"

DOCA_LOG_REGISTER(APSH_APP::Core);

/* This value is guaranteed to be 253 on Linux, and 16 bytes on Windows */
#define MAX_HOSTNAME_LEN 253

#define MAX_LOCAL_PROPERTY_LEN 128
#define MAX_REMOTE_PROPERTY_LEN 128

static void
pid_callback(void *config, void *param)
{
	struct apsh_config *conf = (struct apsh_config *)config;

	conf->pid = (unsigned int)*(int *)param;
}

static void
hash_map_callback(void *config, void *param)
{
	struct apsh_config *conf = (struct apsh_config *)config;
	size_t size = sizeof(conf->exec_hash_map_path);

	if (strnlen(param, size) >= size)
		APP_EXIT("Execute hash map argument too long. Must be <=%zu long.", size - 1);
	strcpy(conf->exec_hash_map_path, param);

	if (access(conf->exec_hash_map_path, F_OK) == -1)
		APP_EXIT("Execute hash map file not found %s", conf->exec_hash_map_path);
}

static void
memr_callback(void *config, void *param)
{
	struct apsh_config *conf = (struct apsh_config *)config;
	size_t size = sizeof(conf->system_mem_region_path);

	if (strnlen(param, size) >= size)
		APP_EXIT("System memory regions map argument too long. Must be <=%zu long.", size - 1);
	strcpy(conf->system_mem_region_path, param);

	if (access(conf->system_mem_region_path, F_OK) == -1)
		APP_EXIT("System memory regions map json file not found %s", conf->system_mem_region_path);
}

static void
vuid_callback(void *config, void *param)
{
	struct apsh_config *conf = (struct apsh_config *)config;
	size_t size = sizeof(conf->system_vuid);

	if (strnlen(param, size) >= size)
		APP_EXIT("system VUID argument too long. Must be <=%zu long.", size - 1);
	strcpy(conf->system_vuid, param);
}

static void
dma_callback(void *config, void *param)
{
	struct apsh_config *conf = (struct apsh_config *)config;
	size_t size = sizeof(conf->dma_dev_name);

	if (strnlen(param, size) >= size)
		APP_EXIT("DMA device name argument too long. Must be <=%zu long.", size - 1);
	strcpy(conf->dma_dev_name, param);
}

static void
os_syms_callback(void *config, void *param)
{
	struct apsh_config *conf = (struct apsh_config *)config;
	size_t size = sizeof(conf->system_os_symbol_map_path);

	if (strnlen(param, size) >= size)
		APP_EXIT("System os symbols map argument too long. Must be <=%zu long.", size - 1);
	strcpy(conf->system_os_symbol_map_path, param);

	if (access(conf->system_os_symbol_map_path, F_OK) == -1)
		APP_EXIT("System os symbols map json file not found %s", conf->system_os_symbol_map_path);
}

static void
os_type_callback(void *config, void *param)
{
	struct apsh_config *conf = (struct apsh_config *)config;
	char *str_param = (char *)param;

	if (!strcasecmp(str_param, "windows"))
		conf->os_type = DOCA_APSH_SYSTEM_WINDOWS;
	else if (!strcasecmp(str_param, "linux"))
		conf->os_type = DOCA_APSH_SYSTEM_LINUX;
	else
		APP_EXIT("OS type is not windows/linux (case insensitive)");
}

static void
time_callback(void *config, void *param)
{
	struct apsh_config *conf = (struct apsh_config *)config;

	conf->time_interval = *(int *)param;
}

void
register_apsh_params()
{
	struct doca_argp_param pid_param = {.short_flag = "p",
					     .long_flag = "pid",
					     .arguments = NULL,
					     .description = "Process ID of process to be attested",
					     .callback = pid_callback,
					     .arg_type = DOCA_ARGP_TYPE_INT,
					     .is_mandatory = true,
					     .is_cli_only = false};
	struct doca_argp_param hash_map_param = {.short_flag = "e",
						  .long_flag = "ehm",
						  .arguments = "<path>",
						  .description = "Exec hash map path",
						  .callback = hash_map_callback,
						  .arg_type = DOCA_ARGP_TYPE_STRING,
						  .is_mandatory = true,
						  .is_cli_only = false};
	struct doca_argp_param memr_param = {.short_flag = "m",
					      .long_flag = "memr",
					      .arguments = "<path>",
					      .description = "System memory regions map",
					      .callback = memr_callback,
					      .arg_type = DOCA_ARGP_TYPE_STRING,
					      .is_mandatory = true,
					      .is_cli_only = false};
	struct doca_argp_param vuid_param = {.short_flag = "f",
					     .long_flag = "vuid",
					     .arguments = NULL,
					     .description = "VUID of the System device",
					     .callback = vuid_callback,
					     .arg_type = DOCA_ARGP_TYPE_STRING,
					     .is_mandatory = true,
					     .is_cli_only = false};
	struct doca_argp_param dma_param = {.short_flag = "d",
					     .long_flag = "dma",
					     .arguments = NULL,
					     .description = "DMA device name",
					     .callback = dma_callback,
					     .arg_type = DOCA_ARGP_TYPE_STRING,
					     .is_mandatory = true,
					     .is_cli_only = false};
	struct doca_argp_param os_syms_param = {.short_flag = "o",
						 .long_flag = "osym",
						 .arguments = "<path>",
						 .description = "System os symbol map path",
						 .callback = os_syms_callback,
						 .arg_type = DOCA_ARGP_TYPE_STRING,
						 .is_mandatory = true,
						 .is_cli_only = false};
	struct doca_argp_param os_type_param = {.short_flag = "s",
						 .long_flag = "osty",
						 .arguments = "[windows|linux]",
						 .description = "System os type - windows/linux",
						 .callback = os_type_callback,
						 .arg_type = DOCA_ARGP_TYPE_STRING,
						 .is_mandatory = true,
						 .is_cli_only = false};
	struct doca_argp_param time_param = {.short_flag = "t",
					      .long_flag = "time",
					      .arguments = "<seconds>",
					      .description = "Scan time interval in seconds",
					      .callback = time_callback,
					      .arg_type = DOCA_ARGP_TYPE_INT,
					      .is_mandatory = true,
					      .is_cli_only = false};

	doca_argp_register_param(&pid_param);
	doca_argp_register_param(&hash_map_param);
	doca_argp_register_param(&memr_param);
	doca_argp_register_param(&vuid_param);
	doca_argp_register_param(&dma_param);
	doca_argp_register_param(&os_syms_param);
	doca_argp_register_param(&os_type_param);
	doca_argp_register_param(&time_param);
	doca_argp_register_version_callback(sdk_version_callback);
}

int
open_doca_device_with_property(enum doca_devinfo_property property, uint8_t *value, size_t val_size,
			       struct doca_dev **retval)
{
	struct doca_devinfo **dev_list;
	uint32_t nb_devs;
	uint8_t val_copy[MAX_LOCAL_PROPERTY_LEN] = {};
	uint8_t buf[MAX_LOCAL_PROPERTY_LEN] = {};
	int ret = DOCA_SUCCESS;
	size_t i;

	*retval = NULL;
	if (val_size > MAX_LOCAL_PROPERTY_LEN) {
		DOCA_LOG_ERR("Value size too large. Ignored.");
		return -1;
	}

	ret = doca_devinfo_list_create(&dev_list, &nb_devs);
	if (ret != DOCA_SUCCESS)
		APP_EXIT("Failed to load doca devices list. Doca_error value: %d", ret);

	/* Prepare value */
	memcpy(val_copy, value, val_size);
	switch (property) {
	case DOCA_DEVINFO_PROPERTY_IFACE_NAME:
		val_size = 32;
		break;
	case DOCA_DEVINFO_PROPERTY_IBDEV_NAME:
		val_size = 64;
		break;
	case DOCA_DEVINFO_PROPERTY_VUID:
		val_size = 128;
		break;
	default:
		break;
	}

	/* Search */
	for (i = 0; i < nb_devs; i++) {
		ret = doca_devinfo_property_get(dev_list[i], property, buf, val_size);
		if (ret == DOCA_SUCCESS && memcmp(buf, val_copy, val_size) == 0) {
			ret = doca_dev_open(dev_list[i], retval);
			if (ret == DOCA_SUCCESS)
				break;
		}
	}

	doca_devinfo_list_destroy(dev_list);
	return ret;
}

int
open_remote_doca_device_with_property(struct doca_dev *local, enum doca_dev_remote_filter filter,
				      enum doca_devinfo_remote_property property, uint8_t *value, size_t val_size,
				      struct doca_dev_remote **retval)
{
	uint32_t nb_rdevs = 0;
	struct doca_devinfo_remote **remote_dev_list = NULL;
	uint8_t val_copy[MAX_REMOTE_PROPERTY_LEN] = {};
	uint8_t buf[MAX_REMOTE_PROPERTY_LEN] = {};
	int ret;
	size_t i;

	*retval = NULL;
	if (val_size > MAX_REMOTE_PROPERTY_LEN) {
		DOCA_LOG_ERR("Value size too large. Ignored.");
		return -1;
	}

	/* Prepare value */
	memcpy(val_copy, value, val_size);
	switch (property) {
	case DOCA_DEVINFO_REMOTE_PROPERTY_VUID:
		val_size = 128;
		break;
	default:
		break;
	}

	/* Search */
	ret = doca_devinfo_remote_list_create(local, filter, &remote_dev_list, &nb_rdevs);
	if (ret)
		APP_EXIT("Failed to create devinfo remote list. Remote devices are available on BF only, do not run on Host.");

	for (i = 0; i < nb_rdevs; i++) {
		ret = doca_devinfo_remote_property_get(remote_dev_list[i], property, buf, sizeof(buf));
		if (ret == DOCA_SUCCESS && memcmp(buf, val_copy, val_size) == 0) {
			ret = doca_dev_remote_open(remote_dev_list[i], retval);
			if (ret == DOCA_SUCCESS)
				break;
		}
	}

	doca_devinfo_remote_list_destroy(remote_dev_list);
	return ret;
}

static struct doca_apsh_ctx *
apsh_ctx_init(struct apsh_config *conf, struct apsh_resources *resources)
{
	int res;

	/* Create a new apsh context */
	struct doca_apsh_ctx *apsh_ctx = doca_apsh_create();

	if (apsh_ctx == NULL)
		APP_EXIT("Create lib APSH context failed");

	/* Get dma device */
	res = open_doca_device_with_property(DOCA_DEVINFO_PROPERTY_IBDEV_NAME, (uint8_t *)conf->dma_dev_name,
					     strlen(conf->dma_dev_name), &resources->dma_device);
	if (res)
		APP_EXIT("Failed to open dma device");

	/* Start apsh context */
	/* set the DMA device */
	res = doca_apsh_dma_dev_set(apsh_ctx, resources->dma_device);
	if (res) {
		app_shield_agent_cleanup(resources);
		APP_EXIT("Set dma device failed");
	}

	/* Start apsh handler and init connection to devices */
	res = doca_apsh_start(apsh_ctx);
	if (res) {
		app_shield_agent_cleanup(resources);
		APP_EXIT("Start APSH failed");
	}

	/* return value */
	return apsh_ctx;
}

static struct doca_apsh_system *
apsh_system_init(struct apsh_config *conf, struct apsh_resources *resources)
{
	int res;
	struct doca_apsh_system *apsh_system;

	res = open_remote_doca_device_with_property(resources->dma_device, DOCA_DEV_REMOTE_FILTER_NET,
						    DOCA_DEVINFO_REMOTE_PROPERTY_VUID, (uint8_t *)conf->system_vuid,
						    strlen(conf->system_vuid), &resources->system_device);
	if (res != DOCA_SUCCESS) {
		app_shield_agent_cleanup(resources);
		APP_EXIT("Failed to open remote device");
	}

	/* Create a new system handler to introspect */
	apsh_system = doca_apsh_system_create(resources->ctx);
	if (apsh_system == NULL) {
		app_shield_agent_cleanup(resources);
		APP_EXIT("Create system context failed");
	}

	/* Start system context - baremetal */
	/* Set the system os symbol map */
	res = doca_apsh_sys_os_symbol_map_set(apsh_system, conf->system_os_symbol_map_path);
	if (res) {
		app_shield_agent_cleanup(resources);
		APP_EXIT("Set os symbols map failed");
	}

	/* Set the system memory region the apsh handler is allowed to access */
	res = doca_apsh_sys_mem_region_set(apsh_system, conf->system_mem_region_path);
	if (res) {
		app_shield_agent_cleanup(resources);
		APP_EXIT("Set mem regions map failed");
	}

	/* Set the system device for the apsh handler to use */
	res = doca_apsh_sys_dev_set(apsh_system, resources->system_device);
	if (res) {
		app_shield_agent_cleanup(resources);
		APP_EXIT("Set system device failed");
	}

	/* Set the system os type - linux/widows */
	res = doca_apsh_sys_os_type_set(apsh_system, conf->os_type);
	if (res) {
		app_shield_agent_cleanup(resources);
		APP_EXIT("Set system os type failed");
	}

	/* Start system handler and init connection to the system and the devices. */
	res = doca_apsh_system_start(apsh_system);
	if (res) {
		app_shield_agent_cleanup(resources);
		APP_EXIT("Start system failed");
	}

	/* return value */
	return apsh_system;
}

void
app_shield_agent_init(struct apsh_config *conf, struct apsh_resources *resources)
{
	/* Init basic apsh handlers */
	memset(resources, 0, sizeof(*resources));
	resources->ctx = apsh_ctx_init(conf, resources);
	resources->sys = apsh_system_init(conf, resources);
}

void
app_shield_agent_cleanup(struct apsh_resources *resources)
{
	/* free the system handler and disconnect from the devices */
	if (resources->sys != NULL)
		doca_apsh_system_destroy(resources->sys);

	/* free the apsh handler and disconnect from the devices */
	if (resources->ctx != NULL)
		doca_apsh_destroy(resources->ctx);

	/* Close the devices */
	if (resources->dma_device != NULL)
		doca_dev_close(resources->dma_device);
	if (resources->system_device != NULL)
		doca_dev_remote_close(resources->system_device);
}

struct doca_apsh_process *
get_process_by_pid(struct doca_apsh_process ***pslist, struct apsh_resources *resources, struct apsh_config *apsh_conf)
{
	struct doca_apsh_process **processes;
	int proc_count, res;
	size_t process_idx;
	typeof(apsh_conf->pid) cur_proc_pid = 0;
	struct doca_apsh_process *process = NULL;

	/* Create list of processes on remote system */
	res = doca_apsh_processes_get(resources->sys, &processes, &proc_count);
	if (res == 0)
		*pslist = processes;
	else {
		app_shield_agent_cleanup(resources);
		APP_EXIT("Get processes failed");
	}

	/* Search for the process 'pid' */
	for (process_idx = 0; process_idx < proc_count; process_idx++) {
		cur_proc_pid = doca_apsh_proc_info_get(processes[process_idx], DOCA_APSH_PROCESS_PID);

		if (apsh_conf->pid == cur_proc_pid) {
			process = processes[process_idx];
			break;
		}
	}
	if (process == NULL) {
		doca_apsh_processes_free(processes);
		app_shield_agent_cleanup(resources);
		APP_EXIT("Process (%d) was not found", apsh_conf->pid);
	}

	*pslist = processes;
	return process;
}

static int
telemetry_register_attest_event(void *schema, doca_telemetry_type_index_t *type_index)
{
	/* Event type for schema. Should be consistent with event struct. */
	doca_telemetry_field_info_t attestation_event_fields[] = {
		{ "timestamp",		"Event timestamp",	DOCA_TELEMETRY_FIELD_TYPE_TIMESTAMP, 1 },
		{ "pid",		"Pid",			DOCA_TELEMETRY_FIELD_TYPE_INT32,     1 },
		{ "result",		"Result",		DOCA_TELEMETRY_FIELD_TYPE_INT32,     1 },
		{ "scan_count",		"Scan Count",		DOCA_TELEMETRY_FIELD_TYPE_UINT64,    1 },
		{ "path",		"Path",			DOCA_TELEMETRY_FIELD_TYPE_CHAR, MAX_PATH},
	};

	/* Register type */
	return doca_telemetry_schema_add_type(schema, "attestation_event", attestation_event_fields,
				       NUM_OF_DOCA_FIELDS(attestation_event_fields), type_index);
}

int
telemetry_start(void **telemetry_schema, void **telemetry_source, struct event_indexes *indices)
{
	int retval;
	void *schema = NULL, *source = NULL;
	/* Enable file write during the app development.
	 * Check written files under data root to make sure that data format is correct.
	 * Default max_file_size is 1 Mb, default max_file_age is 1 hour.
	 */
	struct doca_telemetry_file_write_attr_t file_write = {.max_file_size = 1 * 1024 * 1024,
							      .max_file_age = 60 * 60 * 1000000L,
							      .file_write_enabled = true};
	char source_id_buf[MAX_HOSTNAME_LEN + 1], source_tag_buf[MAX_HOSTNAME_LEN + strlen("_tag") + 1];
	struct doca_telemetry_source_name_attr_t source_attr = { .source_id = source_id_buf,
								 .source_tag = source_tag_buf };

	/* Creating telemetry schema */
	schema = doca_telemetry_schema_init("app_shield_agent_telemetry");
	if (schema == NULL) {
		DOCA_LOG_ERR("Failed to init the doca telemetry schema");
		return -1;
	}

	/* Register all currently supported events */
	retval = telemetry_register_attest_event(schema, &indices->attest_index);
	if (retval < 0) {
		DOCA_LOG_ERR("Failed to register attestation event in the telemetry schema");
		goto schema_error;
	}

	/* Set telemetry file output enabled - useful for debugging only */
	doca_telemetry_schema_file_write_attr_set(schema, &file_write);

	/* Activate the schema */
	retval = doca_telemetry_schema_start(schema);
	if (retval < 0) {
		DOCA_LOG_ERR("Failed to start the doca telemetry schema");
		goto schema_error;
	}

	/* Open a telemetry connection with custom source id and tag */
	source = doca_telemetry_source_create(schema);
	if (source == NULL) {
		DOCA_LOG_ERR("Failed to create a source end point to the telemetry");
		retval = -1;
		goto schema_error;
	}

	/* Creating a unique tag and id per host */
	if (gethostname(source_id_buf, sizeof(source_id_buf)) < 0)  {
		DOCA_LOG_ERR("Gethostname failed, can't create a unique source tag and id");
		retval = -1;
		goto source_error;
	}
	strlcpy(source_tag_buf, source_id_buf, sizeof(source_tag_buf));
	strlcat(source_tag_buf, "_tag", sizeof(source_tag_buf));
	doca_telemetry_source_name_attr_set(source, &source_attr);

	/* Initiate the DOCA telemetry source */
	retval = doca_telemetry_source_start(source);
	if (retval < 0) {
		DOCA_LOG_ERR("Failed to establish a source connection to the telemetry");
		goto source_error;
	}

	/* Success init, return handlers */
	*telemetry_schema = schema;
	*telemetry_source = source;
	return retval;

source_error:
	doca_telemetry_source_destroy(source);
schema_error:
	doca_telemetry_schema_destroy(schema);
	return retval;
}

void
telemetry_destroy(void *telemetry_schema, void *telemetry_source)
{
	doca_telemetry_source_destroy(telemetry_source);
	doca_telemetry_schema_destroy(telemetry_schema);
}
