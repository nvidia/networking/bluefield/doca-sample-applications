/*
 * Copyright (c) 2022 NVIDIA CORPORATION & AFFILIATES, ALL RIGHTS RESERVED.
 *
 * This software product is a proprietary product of NVIDIA CORPORATION &
 * AFFILIATES (the "Company") and all right, title, and interest in and to the
 * software product, including all associated intellectual property rights, are
 * and shall remain exclusively with the Company.
 *
 * This software product is governed by the End User License Agreement
 * provided with the software product.
 *
 */
#include <string.h>

#include <doca_apsh.h>
#include <doca_apsh_attr.h>

#include "apsh_common.h"

#define MAX_LOCAL_PROPERTY_LEN 128
#define MAX_REMOTE_PROPERTY_LEN 128

struct doca_dev *dma_device;
struct doca_dev_remote *pci_device;

int
open_doca_device_with_property(enum doca_devinfo_property property, uint8_t *value, size_t val_size,
			       struct doca_dev **retval)
{
	struct doca_devinfo **dev_list;
	uint32_t nb_devs;
	uint8_t val_copy[MAX_LOCAL_PROPERTY_LEN] = {};
	uint8_t buf[MAX_LOCAL_PROPERTY_LEN] = {};
	int ret = DOCA_SUCCESS, device_not_found = 1;
	size_t i;

	*retval = NULL;
	if (val_size > MAX_LOCAL_PROPERTY_LEN)
		return -1;

	ret = doca_devinfo_list_create(&dev_list, &nb_devs);
	if (ret != DOCA_SUCCESS)
		return -1;

	/* Prepare value */
	memcpy(val_copy, value, val_size);
	switch (property) {
	case DOCA_DEVINFO_PROPERTY_IFACE_NAME:
		val_size = 32;
		break;
	case DOCA_DEVINFO_PROPERTY_IBDEV_NAME:
		val_size = 64;
		break;
	case DOCA_DEVINFO_PROPERTY_VUID:
		val_size = 128;
		break;
	default:
		break;
	}

	/* Search */
	for (i = 0; i < nb_devs; i++) {
		ret = doca_devinfo_property_get(dev_list[i], property, buf, val_size);
		if (ret == DOCA_SUCCESS && memcmp(buf, val_copy, val_size) == 0) {
			ret = doca_dev_open(dev_list[i], retval);
			if (ret == DOCA_SUCCESS) {
				device_not_found = 0;
				break;
			}
		}
	}

	/* Cleanup */
	doca_devinfo_list_destroy(dev_list);

	if (ret == DOCA_SUCCESS && device_not_found)
		return -1;
	return ret;
}

int
open_remote_doca_device_with_property(struct doca_dev *local, enum doca_dev_remote_filter filter,
				      enum doca_devinfo_remote_property property, uint8_t *value, size_t val_size,
				      struct doca_dev_remote **retval)
{
	uint32_t nb_rdevs = 0;
	struct doca_devinfo_remote **remote_dev_list = NULL;
	uint8_t val_copy[MAX_REMOTE_PROPERTY_LEN] = {};
	uint8_t buf[MAX_REMOTE_PROPERTY_LEN] = {};
	int ret, device_not_found = 1;
	size_t i;

	*retval = NULL;
	if (val_size > MAX_REMOTE_PROPERTY_LEN)
		return -1;

	/* Prepare value */
	memcpy(val_copy, value, val_size);
	switch (property) {
	case DOCA_DEVINFO_REMOTE_PROPERTY_VUID:
		val_size = 128;
		break;
	default:
		break;
	}

	/* Search */
	ret = doca_devinfo_remote_list_create(local, filter, &remote_dev_list, &nb_rdevs);
	if (ret)
		return -1;

	for (i = 0; i < nb_rdevs; i++) {
		ret = doca_devinfo_remote_property_get(remote_dev_list[i], property, buf, sizeof(buf));
		if (ret == DOCA_SUCCESS && memcmp(buf, val_copy, val_size) == 0) {
			ret = doca_dev_remote_open(remote_dev_list[i], retval);
			if (ret == DOCA_SUCCESS) {
				device_not_found = 0;
				break;
			}
		}
	}

	doca_devinfo_remote_list_destroy(remote_dev_list);

	if (ret == DOCA_SUCCESS && device_not_found)
		return -1;
	return ret;
}

int
init_doca_apsh(struct doca_apsh_ctx **ctx, const char *dma_device_name)
{
	int res;
	struct doca_apsh_ctx *apsh_ctx;

	/* Get dma device */
	res = open_doca_device_with_property(DOCA_DEVINFO_PROPERTY_IBDEV_NAME, (uint8_t *)dma_device_name,
					     strlen(dma_device_name), &dma_device);
	if (res != DOCA_SUCCESS)
		return -1;

	/* Init apsh library */
	apsh_ctx = doca_apsh_create();
	if (apsh_ctx == NULL) {
		doca_dev_close(dma_device);
		return -1;
	}

	/* set the DMA device */
	res = doca_apsh_dma_dev_set(apsh_ctx, dma_device);
	if (res) {
		cleanup_doca_apsh(apsh_ctx, NULL);
		return res;
	}

	/* Start apsh context */
	res = doca_apsh_start(apsh_ctx);
	if (res) {
		cleanup_doca_apsh(apsh_ctx, NULL);
		return -1;
	}

	*ctx = apsh_ctx;
	return res;
}

int
init_doca_apsh_system(struct doca_apsh_ctx *ctx, enum doca_apsh_system_os os_type, const char *os_symbols,
		      const char *mem_region, const char *pci_vuid, struct doca_apsh_system **system)
{
	int res;
	struct doca_apsh_system *sys;

	/* Get pci device that connects to the system */
	res = open_remote_doca_device_with_property(dma_device, DOCA_DEV_REMOTE_FILTER_NET,
						    DOCA_DEVINFO_REMOTE_PROPERTY_VUID, (uint8_t *)pci_vuid,
						    strlen(pci_vuid), &pci_device);
	if (res != DOCA_SUCCESS) {
		cleanup_doca_apsh(ctx, NULL);
		return -1;
	}

	/* Create a new system handler to introspect */
	sys = doca_apsh_system_create(ctx);
	if (sys == NULL) {
		cleanup_doca_apsh(ctx, NULL);
		return -1;
	}

	/* Set the system os type - linux/widows */
	res = doca_apsh_sys_os_type_set(sys, os_type);
	if (res) {
		cleanup_doca_apsh(ctx, sys);
		return res;
	}

	/* Set the system os symbol map */
	res = doca_apsh_sys_os_symbol_map_set(sys, os_symbols);
	if (res) {
		doca_apsh_system_destroy(sys);
		return res;
	}

	/* Set the system memory region the apsh handler is allowed to access */
	res = doca_apsh_sys_mem_region_set(sys, mem_region);
	if (res) {
		cleanup_doca_apsh(ctx, sys);
		return res;
	}

	/* Set the system device for the apsh library to use */
	res = doca_apsh_sys_dev_set(sys, pci_device);
	if (res) {
		cleanup_doca_apsh(ctx, sys);
		return res;
	}

	/* Start system handler and init connection to the system and the devices. */
	res = doca_apsh_system_start(sys);
	if (res) {
		cleanup_doca_apsh(ctx, sys);
		return res;
	}

	*system = sys;
	return res;
}

void
cleanup_doca_apsh(struct doca_apsh_ctx *ctx, struct doca_apsh_system *system)
{
	doca_apsh_destroy(ctx);
	if (system != NULL)
		doca_apsh_system_destroy(system);
	if (pci_device != NULL)
		doca_dev_remote_close(pci_device);
	if (dma_device != NULL)
		doca_dev_close(dma_device);
}

int
process_get(DOCA_APSH_PROCESS_PID_TYPE pid, struct doca_apsh_system *sys, struct doca_apsh_process ***processes,
	    struct doca_apsh_process **process)
{
	struct doca_apsh_process **pslist;
	int num_processes, i;

	*process = NULL;

	/* Read host processes list */
	if (doca_apsh_processes_get(sys, &pslist, &num_processes) != 0)
		return -1;

	/* Search for the requested process */
	for (i = 0; i < num_processes; ++i) {
		if (doca_apsh_proc_info_get(pslist[i], DOCA_APSH_PROCESS_PID) == pid) {
			*process = pslist[i];
			break;
		}
	}

	/* Should not release the pslist, it will also release the memory of the requested process (with "pid"). */
	*processes = pslist;
	return num_processes;
}
