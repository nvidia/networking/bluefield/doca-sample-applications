/*
 * Copyright (c) 2022 NVIDIA CORPORATION & AFFILIATES, ALL RIGHTS RESERVED.
 *
 * This software product is a proprietary product of NVIDIA CORPORATION &
 * AFFILIATES (the "Company") and all right, title, and interest in and to the
 * software product, including all associated intellectual property rights, are
 * and shall remain exclusively with the Company.
 *
 * This software product is governed by the End User License Agreement
 * provided with the software product.
 *
 */
#ifndef APSH_COMMON_H_
#define APSH_COMMON_H_

#include <doca_apsh.h>

/* Creates and starts a DOCA Apsh context, in order to make the library usable.
 *
 * On successful 0 is returned. On failure, return value is non-zero and all lib Apsh resources are
 * freed. */
int init_doca_apsh(struct doca_apsh_ctx **ctx, const char *dma_device_name);

/* Creates and starts a DOCA Apsh System context, in order to apply the library on a specific host system.
 *
 * On successful 0 is returned. On failure, return value is non-zero and all lib Apsh resources are
 * freed (including Apsh context). */
int init_doca_apsh_system(struct doca_apsh_ctx *ctx, enum doca_apsh_system_os os_type, const char *os_symbols,
			  const char *mem_region, const char *pci_vuid, struct doca_apsh_system **system);

/* Destroys the system and context handler and free inner resources. */
void cleanup_doca_apsh(struct doca_apsh_ctx *ctx, struct doca_apsh_system *system);

/* Searches for a process on the "sys" with a specific PID and returns the entire processes list and a pointer to
 * the found process. If process was not found, *process will be NULL.
 *
 * On successful creation of the processes list the return value its size. On failure, return value is negative. */
int process_get(DOCA_APSH_PROCESS_PID_TYPE pid, struct doca_apsh_system *sys,
		struct doca_apsh_process ***processes, struct doca_apsh_process **process);

#endif /* APSH_COMMON_H_ */
